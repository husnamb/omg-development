﻿<%@ WebService Language="VB" Class="UnifiOMGSystem" %>

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://omg.hypp.tv/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class UnifiOMGSystem
    Inherits System.Web.Services.WebService

    Dim MyUnifiFunction As New UnifiPortal_Function
    Dim MyOMGFunction As New OMG_Function
    Dim MyTMFunction As New TMBilling_Function
    Dim MyMerchant As New MerchantDetails
    Dim b4hashstring As String
    Dim MySignature As String
    Dim APILog As New INT_APILog
    Dim ErrorMessage As String = ""

    <WebMethod(Description:="This get OTT product list and customer subscription status.")>
    Public Function OMG_RetrieveOTTServices(ByVal MyRequest As OMG_ServiceReq) As OMG_ServiceResp

        Dim MyResponse As New OMG_ServiceResp

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID 
            If (Trim(MyRequest.TM_AccountID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    'Get ProductList & Product
                    Dim MyProductList As New ProductListService_ID
                    Dim MyProduct As New ProductService_ID
                    'Get Product Details
                    Dim ProductResponse As New ProductDetailsRes
                    Dim ProductRequest As New ProductDetailsReq
                    'Status Dictionary
                    Dim MyStatusDict As New Dictionary(Of Integer, String)

                    'Split OTT Service ID to Get ProductList
                    Dim ListOTT_ServiceID() As String = Split(MyRequest.OTT_ServiceID, ",")
                    Dim INTProductListService_ID(ListOTT_ServiceID.Length - 1) As ProductListService_ID

                    'Array OTT Service ID
                    For i As Integer = 0 To ListOTT_ServiceID.Length - 1

                        Dim x As New ProductListService_ID()
                        'Counter for Countlist
                        Dim Count As Integer = 0
                        x.OTT_ServiceID = ListOTT_ServiceID(i)

                        'Get Product under Productlist
                        ProductRequest.TM_AccountID = MyRequest.TM_AccountID
                        ProductRequest.OTT_ServiceIID = ListOTT_ServiceID(i)
                        ProductResponse = MyUnifiFunction.GetProductDetails(ProductRequest)

                        'Product Status
                        If MyStatusDict.ContainsKey(ProductResponse.ResponseCode) = False Then
                            MyStatusDict.Add(ProductResponse.ResponseCode, ProductResponse.ResponseMsg)
                        End If

                        'Product Datatable
                        If ProductResponse.ResponseCode = 0 Then

                            'Reset Counter for Countlist
                            If ListOTT_ServiceID(i) <> ListOTT_ServiceID(i) Then
                                Count = 0
                            End If

                            Dim INTProductService_ID(ProductResponse.ResponseDataTable.Rows.Count - 1) As ProductService_ID
                            For j As Integer = 0 To ProductResponse.ResponseDataTable.Rows.Count - 1

                                Dim y As New ProductService_ID()
                                y.ProductID = ProductResponse.ResponseDataTable.Rows(j)("tm_productCode")
                                y.Name = ProductResponse.ResponseDataTable.Rows(j)("productName")
                                y.Status = ProductResponse.ResponseDataTable.Rows(j)("status_ID")
                                y.Price = ProductResponse.ResponseDataTable.Rows(j)("productPrice")
                                y.SubscriptionDate = ProductResponse.ResponseDataTable.Rows(j)("subscription_date").ToString
                                Count = Count + 1
                                INTProductService_ID(j) = y

                            Next
                            x.CountTotal = Count
                            x.Product = INTProductService_ID
                        Else
                            x.CountTotal = Count
                        End If

                        INTProductListService_ID(i) = x

                    Next

                    MyResponse.ProductList = INTProductListService_ID

                    'Response Code
                    For Each Status As KeyValuePair(Of Integer, String) In MyStatusDict

                        If Status.Key = 0 Then
                            MyResponse.ResponseCode = Status.Key
                            MyResponse.ResponseMsg = Status.Value
                            Exit For 'Force Break if found 0

                        ElseIf Status.Key = 103 Then
                            MyResponse.ResponseCode = Status.Key
                            MyResponse.ResponseMsg = Status.Value
                            Exit For 'Force Break if found 103
                        Else
                            MyResponse.ResponseCode = Status.Key
                            MyResponse.ResponseMsg = Status.Value
                        End If

                    Next

                Else
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If

            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If

        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select

        End If

        MyResponse.OTT_ID = MyRequest.OTT_ID
        MyResponse.TM_AccountType = MyRequest.TM_AccountType
        MyResponse.TM_AccountID = MyRequest.TM_AccountID

        'APILog
        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = 0
        APILog.EventName = "OMG_RetrieveOTTServices"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)


        Return MyResponse

    End Function

    <WebMethod(Description:="To create order for new subscription or termination.")>
    Public Function OMG_NewServiceOrder(ByVal MyRequest As OMG_OrderReq) As OMG_OrderResp

        Dim MyResponse As New OMG_OrderResp
        Dim MyTMProduct As New TMProductDetails
        Dim ValidateUserReq As New INT_ValidateUserReq
        Dim ValidateUserResp As New INT_ValidateUserResp
        Dim TokenRequest As New OMG_TokenReq
        Dim NewOttOrder As Boolean = False
        Dim OTTTerminate As Boolean = False
        Dim OMG_TXNID As String
        Dim OrderMode As Integer
        Dim ProceedOrder As Boolean = False
        Dim CalculateServiceDay As Boolean = False
        Dim ServiceDay As Integer = 0
        Dim BillCycleDate As String = ""
        Dim ProceedTermination As Boolean = False

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                'Sha1 “&&” +OTT_ID+ “##” + OTT_PWD + “##” + TM_AccountID + “&&”
                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)
                    MyOMGFunction.GetAccount(MyRequest.TM_AccountID, MyRequest.TM_AccountType)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Check product EXIST & ACTIVE
                        MyTMProduct = MyTMFunction.GetTMProductDetails("", MyRequest.ProductID, "")

                        If MyTMProduct.ResponseCode = 0 And
                            UCase(MyTMProduct.Status) = "ACTIVE" And
                            ((MyTMProduct.ProductType = "ALA-CARTE-ANNUALLY") Or (MyTMProduct.ProductType = "ALA-CARTE-MONTHLY")) Then

                            'For APILog
                            APILog.OMGProductID = MyTMProduct.OMGProductID

                            '6. Check Order Type
                            'Order Type : 0 - Subscription 1 - Termination
                            If MyRequest.OrderType = 0 Then
                                NewOttOrder = True
                            ElseIf MyRequest.OrderType = 1 Then
                                OTTTerminate = True
                            Else
                                MyResponse.ResponseCode = 106
                                MyResponse.ResponseMsg = "Failed order request"
                            End If

                            'Process subscribe 1.validate acct 2.Generate token 3.create omg txn id 4.check subscription 5.order
                            'Process terminate 1.check subscription 2.terminate
                            If NewOttOrder = True Then

                                'Subscribe 1: Request account validation to NOVA / ICP
                                ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
                                ValidateUserReq.TM_AccountType = GetAccountResp.AccountType
                                '-- Need to enable when production -----------------------------
                                '---ValidateUserResp = MyTMFunction.ValidateAccountId(ValidateUserReq)
                                ValidateUserResp = MyTMFunction.TempValidateAccountId(ValidateUserReq)

                                If ValidateUserResp.ResponseCode = "0" Then

                                    If UCase(ValidateUserResp.Status) = "ACTIVE" Then

                                        'Only Unifi Customer allow to subscribe OTT
                                        'If UCase(ValidateUserResp.TM_AccountType) = "UNIFI" Then

                                        'Subscribe 2:Generate Token
                                        TokenRequest.OTT_ID = MyRequest.OTT_ID
                                        TokenRequest.OTT_TXNID = MyRequest.OrderID
                                        TokenRequest.ProductID = MyRequest.ProductID
                                        TokenRequest.Signature = MyRequest.Signature
                                        TokenRequest.TM_AccountID = MyRequest.TM_AccountID

                                        Dim TempDate As DateTime = DateTime.Now
                                        Dim KeyToken As String = TempDate.ToString("yyyyMMddhhmmss")

                                        Dim b4hashToken As String = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + KeyToken + "##" + MyRequest.TM_AccountID + "##" + MyRequest.ProductID + "&&"
                                        Dim HashToken As String = MyOMGFunction.GetEncryption(b4hashToken, True)

                                        Dim GenerateTokenResp As New PostResponse
                                        GenerateTokenResp = MyOMGFunction.SetToken(TokenRequest, GetAccountResp.LoginID, KeyToken, HashToken)

                                        If GenerateTokenResp.ResponseCode = "0" And GenerateTokenResp.Status = "1" Then

                                            Dim SetIdReq As New INT_SetTransactionIDReq
                                            Dim SetIdResp As New INT_SetTransactionIDResp

                                            '-- Need to enable when production -----------------------------
                                            '--SetIdReq.TM_AccountID = ValidateUserResp.TMAccountID
                                            SetIdReq.TM_AccountID = MyRequest.TM_AccountID
                                            SetIdReq.MobileNumber = ValidateUserResp.MobileNumber
                                            SetIdReq.CustomerEmail = ValidateUserResp.CustomerEmail
                                            SetIdReq.Id = ValidateUserResp.Id
                                            SetIdReq.IdType = ValidateUserResp.IdType
                                            SetIdReq.TM_BillCycle = ValidateUserResp.TM_BillCycle
                                            SetIdReq.OTT_TXNID = MyRequest.OrderID
                                            SetIdReq.Token = HashToken

                                            'Subscribe 3: Create OMG_TXNID - purpose to proceed with Order
                                            SetIdResp = MyTMFunction.SetOMGTransactionID(SetIdReq)

                                            If SetIdResp.ResponseCode = "0" Then

                                                If SetIdResp.Status = "1" Then

                                                    OMG_TXNID = SetIdResp.OMG_TXNID
                                                    OrderMode = 5

                                                    'Subscribe 4: Check if user has subscribe to the product
                                                    '- check in ProductSubscription_tbl based on omg_id (ProductValidation)
                                                    '- check in BillingSubscription_tbl based on tm_id (BillingValidation)                                   
                                                    Dim ValidateProduct As New PostResponse
                                                    ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID)

                                                    If ValidateProduct.ResponseCode = 0 Then

                                                        If ValidateProduct.Status = "0" Then

                                                            Dim ValidateBill As New PostResponse
                                                            ValidateBill = MyTMFunction.BillingValidation(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                                                            If ValidateBill.ResponseCode = "0" Then

                                                                If ValidateBill.Status = "0" Then
                                                                    'Subscribe 4: Order
                                                                    ProceedOrder = True
                                                                Else
                                                                    MyResponse.ResponseCode = 106
                                                                    Select Case ValidateBill.Status
                                                                        Case 1
                                                                            MyResponse.ResponseMsg = "Already subscribe to product"
                                                                        Case 2
                                                                            MyResponse.ResponseMsg = "Available order in process"
                                                                    End Select
                                                                End If

                                                                'Subscribe 4: Process OTT order request
                                                                '- Send Order request to NOVA / ICP
                                                                '- Insert in BillingSubscription_tbl & ProductSubscription_tbl
                                                                If ProceedOrder = True Then
                                                                    Dim TempDateOrder As DateTime = DateTime.Now
                                                                    Dim Format As String = "MM/dd/yyyy"
                                                                    Dim ActivationDate As String = TempDateOrder.ToString(Format)

                                                                    Dim TMOrderReq As New INT_NewOrderReq
                                                                    Dim TMOrderResp As New INT_NewOrderResp

                                                                    If MyRequest.OTT_ID = "iflix" Then
                                                                        TMOrderResp.ResponseCode = "0"
                                                                    Else
                                                                        '-- Need to enable when production !!!! -----------------------------
                                                                        'TMOrderReq.ReqType = "Order"
                                                                        'TMOrderReq.TM_AccountID = GetAccountResp.ServiceID
                                                                        'TMOrderReq.TM_AccountType = GetAccountResp.AccountType
                                                                        'TMOrderReq.TMProductID = MyTMProduct.TMProductID
                                                                        'TMOrderReq.TMProductCode = MyTMProduct.TMProductCode
                                                                        'TMOrderReq.TMProductName = MyTMProduct.TMProductName
                                                                        'TMOrderReq.ProductType = MyTMProduct.ProductType
                                                                        'TMOrderReq.ActivationDate = ActivationDate

                                                                        'TMOrderResp = MyTMFunction.NewOTTOrder(TMOrderReq)
                                                                        '---------------------------------------------------------------  
                                                                    End If

                                                                    TMOrderResp.ResponseCode = "0"     '-- For TESTING ONLY!!

                                                                    If TMOrderResp.ResponseCode = "0" Then

                                                                        Dim BillReq As New OMG_SubmitOrderRequest
                                                                        BillReq.TM_AccountID = GetAccountResp.ServiceID
                                                                        BillReq.TM_AccountType = GetAccountResp.AccountType
                                                                        BillReq.TM_MobileNo = SetIdReq.MobileNumber
                                                                        BillReq.TM_Email = SetIdReq.CustomerEmail

                                                                        Dim SubscribeBill As New PostResponse
                                                                        SubscribeBill = MyTMFunction.BillingSubscription(BillReq, MyTMProduct.TMProductID, MyTMProduct.OTTMerchantID, "OMG", SetIdReq.TM_BillCycle)

                                                                        If SubscribeBill.ResponseCode = "0" Then

                                                                            If SubscribeBill.Status = "1" Then

                                                                                Dim SubsProductReq As New ProductSubsReq
                                                                                Dim SubsProductResp As New PostResponse

                                                                                SubsProductReq.ServiceID = GetAccountResp.ServiceID
                                                                                SubsProductReq.LoginID = GetAccountResp.LoginID
                                                                                SubsProductReq.OTT_UserID = ""
                                                                                SubsProductReq.OTTMerchantID = MyTMProduct.OTTMerchantID
                                                                                SubsProductReq.OMGProductID = MyTMProduct.OMGProductID
                                                                                SubsProductReq.TMBillID = SubscribeBill.TMBillID
                                                                                SubsProductReq.TMProductID = MyTMProduct.TMProductID
                                                                                SubsProductReq.OMG_TXNID = OMG_TXNID
                                                                                SubsProductReq.OrderMode = OrderMode
                                                                                'SubsProductReq.TMMerchantID = MyTMProduct.TMMerchantID (TAK PERLU - edit 24/08/2016)

                                                                                SubsProductResp = MyOMGFunction.ProductSubscription(SubsProductReq)

                                                                                If SubsProductResp.ResponseCode = "0" Then

                                                                                    Select Case SubsProductResp.Status
                                                                                        Case 0
                                                                                            MyResponse.ResponseCode = 106
                                                                                            MyResponse.ResponseMsg = "Failed order request"
                                                                                        Case 1
                                                                                            MyResponse.ActivationDate = ActivationDate
                                                                                            MyResponse.ResponseCode = 0
                                                                                            MyResponse.ResponseMsg = "Successful transaction"
                                                                                        Case Else
                                                                                            MyResponse.ResponseCode = 106
                                                                                            MyResponse.ResponseMsg = "Failed order request"
                                                                                            ErrorMessage = "ProductSubscription Status: " & SubsProductResp.Status
                                                                                    End Select
                                                                                Else
                                                                                    MyResponse.ResponseCode = 101
                                                                                    MyResponse.ResponseMsg = "Technical error"
                                                                                    ErrorMessage = "ProductSubscription: " & SubsProductResp.ResponseMsg & "(" & SubsProductResp.ResponseCode & ")"
                                                                                End If
                                                                            Else
                                                                                MyResponse.ResponseCode = 106
                                                                                MyResponse.ResponseMsg = "Failed order request"
                                                                                ErrorMessage = "BillingSubscription Status: " & SubscribeBill.Status
                                                                            End If

                                                                        Else
                                                                            MyResponse.ResponseCode = 101
                                                                            MyResponse.ResponseMsg = "Technical error"
                                                                            ErrorMessage = "BillingSubscription: " & SubscribeBill.ResponseMsg & "(" & SubscribeBill.ResponseCode & ")"
                                                                        End If
                                                                    Else
                                                                        MyResponse.ResponseCode = 106
                                                                        MyResponse.ResponseMsg = "Failed order creation to TM Billing"
                                                                    End If
                                                                End If ' End ProcessOrder


                                                            Else
                                                                MyResponse.ResponseCode = 101
                                                                MyResponse.ResponseMsg = "Technical error"
                                                                ErrorMessage = "BillingValidation: " & ValidateBill.ResponseMsg & "(" & ValidateBill.ResponseCode & ")"
                                                            End If

                                                        Else 'Subscribe 4: ValidateProduct status 0
                                                            MyResponse.ResponseCode = 106
                                                            Select Case ValidateProduct.Status
                                                                Case 3
                                                                    MyResponse.ResponseMsg = "Pending termination"
                                                                Case 5
                                                                    'Get TM Product code
                                                                    Dim MyTMProduct2 As New TMProductDetails
                                                                    MyTMProduct2 = MyTMFunction.GetTMProductDetails("", ValidateProduct.OMGProductCode, "unifi")
                                                                    MyResponse.ResponseMsg = "Active product subscription (" & MyTMProduct2.TMProductCode & ") under the same merchant"
                                                                Case Else   'Status = 1, 4
                                                                    MyResponse.ResponseMsg = "Already subscribe to product"
                                                            End Select
                                                        End If

                                                    Else 'Subscribe 4: ValidateProduct Response 0
                                                        MyResponse.ResponseCode = 101
                                                        MyResponse.ResponseMsg = "Technical error"
                                                        ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                                                    End If

                                                Else 'Subscribe 3:OMG_TXNID Status 1
                                                    MyResponse.ResponseCode = 101
                                                    MyResponse.ResponseMsg = "Technical error"
                                                    ErrorMessage = "SetOMGTransactionID Status: " & SetIdResp.Status
                                                End If

                                            Else 'Subscribe 3: OMG_TXNID Response 0
                                                MyResponse.ResponseCode = 101
                                                MyResponse.ResponseMsg = "Technical error"
                                                ErrorMessage = "OMGID: " & SetIdResp.ResponseMsg & "(" & SetIdResp.ResponseCode & ")"
                                            End If

                                        Else 'Subscribe 2: Generate Token
                                            MyResponse.ResponseCode = 106
                                            If GenerateTokenResp.Status = "2" Then
                                                MyResponse.ResponseMsg = "Invalid / duplicate transaction ID"
                                            Else
                                                MyResponse.ResponseMsg = "Failed transaction"
                                            End If
                                            ErrorMessage = "SetToken: " & GenerateTokenResp.ResponseMsg & "(" & GenerateTokenResp.ResponseCode & ")"
                                        End If

                                        'Else 'Subscribe 1: Validate acct type
                                        'MyResponse.ResponseCode = 106
                                        'MyResponse.ResponseMsg = "TMAccountType system validation is not Unifi"
                                        'End If

                                    Else 'Subscribe 1: Validate acct id
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Account is NOT active"
                                    End If

                                    'Subscribe 1: Validate acct id
                                ElseIf ValidateUserResp.ResponseCode = "103" Then
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Account is NOT active"

                                ElseIf ValidateUserResp.ResponseCode = "106" Then
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Account NOT Exist"
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error/Not Unifi Account Type"
                                    ErrorMessage = "ValidateAccountId: " & ValidateUserResp.ResponseMsg & "(" & ValidateUserResp.ResponseCode & ")"
                                End If

                            End If 'End NewOttOrder

                            If OTTTerminate = True Then

                                'Terminate 1 :Checking user subscription before proceed to process TM billing termination request
                                '- check in ProductSubscription_tbl based on omg_id (ProductValidation) 
                                '- check in BillingSubscription_tbl based on tm_id (BillingValidation)                            
                                Dim ValidateProduct As New PostResponse
                                Dim ValidateBill As New PostResponse
                                ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID, True)

                                If ValidateProduct.ResponseCode = 0 Then

                                    Select Case ValidateProduct.Status
                                        Case 0, 5
                                            MyResponse.ResponseCode = 106
                                            MyResponse.ResponseMsg = "NOT subscribe to product"
                                        Case 3
                                            MyResponse.ResponseCode = 106
                                            MyResponse.ResponseMsg = "Pending termination"
                                        Case 1, 4
                                            ValidateBill = MyTMFunction.BillingValidation(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                                            If ValidateBill.ResponseCode = "0" Then

                                                If ValidateBill.Status = "1" Then
                                                    CalculateServiceDay = True
                                                ElseIf ValidateBill.Status = "2" Then
                                                    MyResponse.ResponseCode = 106
                                                    MyResponse.ResponseMsg = "Order is in process"
                                                Else
                                                    MyResponse.ResponseCode = 106
                                                    MyResponse.ResponseMsg = "Subscription for product " & MyTMProduct.TMProductCode & " NOT EXIST"
                                                End If
                                            Else
                                                MyResponse.ResponseCode = 101
                                                MyResponse.ResponseMsg = "Technical error - BillingValidation"
                                                ErrorMessage = "BillingValidation: " & ValidateBill.ResponseMsg & "(" & ValidateBill.ResponseCode & ")"
                                            End If
                                        Case Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "ProductValidation Status: " & ValidateProduct.Status
                                    End Select
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error - ProductValidation"
                                    ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                                End If

                                'Calculate service day
                                If CalculateServiceDay = True Then

                                    If (ValidateBill.TM_BillCycle = "0") And
                                        (ValidateBill.OSMActivationDate <> "0") Then

                                        'Condition B: Order from SDP, terminate from OTT
                                        '- Need to trigger ValidateAccountId() to get TM_BillCycle
                                        ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
                                        ValidateUserReq.TM_AccountType = GetAccountResp.AccountType
                                        ValidateUserResp = MyTMFunction.ValidateAccountId(ValidateUserReq)

                                        If ValidateUserResp.ResponseCode = "0" And UCase(ValidateUserResp.Status) = "ACTIVE" Then
                                            ServiceDay = MyOMGFunction.CalculateServiceDay(MyTMProduct.ProductType, MyTMProduct.TMProductName, ValidateBill.OSMActivationDate, Integer.Parse(ValidateUserResp.TM_BillCycle))

                                            BillCycleDate = ValidateUserResp.TM_BillCycle
                                        Else
                                            ErrorMessage = "TM: Failed to get TM_BillingCycleDate"
                                        End If

                                    ElseIf (ValidateBill.TM_BillCycle <> "0") And
                                            (ValidateBill.OSMActivationDate = "0") Then

                                        Dim activateDate As String = ValidateProduct.ActivationDate.ToString("MM/dd/yyyy")

                                        'Condition C: Order from OTT, terminate from OTT
                                        'Condition D: Order from OTT, terminate from SDP
                                        ServiceDay = MyOMGFunction.CalculateServiceDay(MyTMProduct.ProductType, MyTMProduct.TMProductName, activateDate, Integer.Parse(ValidateBill.TM_BillCycle))
                                    Else
                                        ServiceDay = 0
                                    End If

                                    If ServiceDay <> 0 Then
                                        ProceedTermination = True
                                    Else
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Service day is NULL"
                                    End If
                                End If

                                'Terminate 2: Termination
                                If ProceedTermination = True Then

                                    'Send termination to NOVA / ICP
                                    Dim TempDateTerminate As DateTime = DateTime.Now
                                    Dim Format As String = "MM/dd/yyyy"
                                    Dim DeactivationDate As String = TempDateTerminate.ToString(Format)

                                    Dim TMOrderReq As New INT_NewOrderReq
                                    Dim TMOrderResp As New INT_NewOrderResp

                                    '-- Need to enable when production -----------------------------
                                    'TMOrderReq.ReqType = "Terminate"
                                    'TMOrderReq.TM_AccountID = GetAccountResp.ServiceID
                                    'TMOrderReq.TM_AccountType = GetAccountResp.AccountType
                                    'TMOrderReq.TMProductID = MyTMProduct.TMProductID
                                    'TMOrderReq.TMProductCode = MyTMProduct.TMProductCode
                                    'TMOrderReq.TMProductName = MyTMProduct.TMProductName
                                    'TMOrderReq.ProductType = MyTMProduct.ProductType
                                    'TMOrderReq.DeactivationDate = DeactivationDate

                                    'TMOrderResp = MyTMFunction.NewOTTOrder(TMOrderReq)
                                    '---------------------------------------------------------------

                                    TMOrderResp.ResponseCode = "0"     '-- For TESTING ONLY!!

                                    If TMOrderResp.ResponseCode = "0" Then

                                        Dim BillTerminateResp As New PostResponse

                                        If (ValidateBill.TM_BillCycle = "0") Then
                                            BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "Terminate", BillCycleDate)
                                        ElseIf ValidateBill.TM_BillCycle <> "0" Then
                                            BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "terminate")
                                        End If

                                        If BillTerminateResp.ResponseCode = "0" Then

                                            If BillTerminateResp.Status = "1" Then

                                                Dim ProdTerminateResp As New PostResponse
                                                ProdTerminateResp = MyOMGFunction.ProductTermination(MyTMProduct.OMGProductID, GetAccountResp.LoginID, "inactive", "client", ServiceDay)

                                                If ProdTerminateResp.ResponseCode = "0" And ProdTerminateResp.Status = "1" Then

                                                    Dim FormatTemp As String = "dd-MM-yyyy HH:mm:ss"
                                                    Dim TempDeactive As DateTime = ProdTerminateResp.TerminationDate
                                                    Dim TerminationDate As String = TempDeactive.ToString(FormatTemp)

                                                    Dim TempServiceEndDate As DateTime = ProdTerminateResp.ServiceEndDate
                                                    Dim ServiceEndDate As String = TempServiceEndDate.ToString(FormatTemp)

                                                    MyResponse.ActivationDate = TerminationDate
                                                    MyResponse.ResponseCode = 0
                                                    MyResponse.ResponseMsg = "Successful Transaction"

                                                Else
                                                    MyResponse.ResponseCode = 106
                                                    MyResponse.ResponseMsg = "Failed termination"
                                                    ErrorMessage = "ProductTermination Status: " & ProdTerminateResp.Status
                                                End If
                                            Else
                                                MyResponse.ResponseCode = 101
                                                MyResponse.ResponseMsg = "Technical error - Stored prod bill termination"
                                                ErrorMessage = "BillingTermination Status: " & BillTerminateResp.Status
                                            End If
                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "BillingTermination: " & BillTerminateResp.ResponseMsg & "(" & BillTerminateResp.ResponseCode & ")"
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Failed termination to TM billing"
                                        'NOTES: Exact error please refer to TMBillingAPILog.
                                    End If
                                End If
                            End If 'End OTTTerminate

                        Else '5.Check product EXIST & ACTIVE
                            Select Case MyTMProduct.ResponseCode
                                Case "0", "1"       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case "2"
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
                            End Select
                        End If

                    Else '4.Check TM_AccountID
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If

                Else '3.Signature
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If

            Else '2.TM_AccountID
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If

        Else '1.Merchant
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        MyResponse.OTT_ID = MyRequest.OTT_ID
        MyResponse.OrderID = MyRequest.OrderID
        MyResponse.OrderType = MyRequest.OrderType
        MyResponse.TM_AccountType = MyRequest.TM_AccountType
        MyResponse.TM_AccountID = MyRequest.TM_AccountID
        MyResponse.ProductID = MyRequest.ProductID

        'APILog
        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = 0
        APILog.EventName = "OMG_NewServiceOrder"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

End Class