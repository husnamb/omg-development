﻿Imports System.IO
Imports System.Security.Cryptography
Imports System.Web.Compilation
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.Net

Public Class UnifiPortal_Function

    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)
    Dim MyOMGFunction As New OMG_Function
    Dim MyTMFunction As New TMBilling_Function
    Dim APILog As New INT_APILog

    Function GetProductDetails(ByVal MyRequest As ProductDetailsReq) As ProductDetailsRes
        'Function GetProductDetails(ByVal TM_AccountID As String, ByVal OTT_ServiceID As String) As DataTable

        Dim MyResponse As New ProductDetailsRes
        Dim DataReader As SqlDataReader
        Dim MyDataTable As New DataTable

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If


            Dim cmd As New SqlCommand("GetTMProductDetailsByTMAccountID", strConn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
            cmd.Parameters.Add("@OTTServiceID", SqlDbType.VarChar, 30).Value = MyRequest.OTT_ServiceIID


            DataReader = cmd.ExecuteReader

            If DataReader.HasRows Then
                MyDataTable.Load(DataReader)
                MyResponse.ResponseDataTable = MyDataTable
                MyResponse.ResponseCode = 0
                MyResponse.ResponseMsg = "Successful"

            Else
                MyResponse.ResponseCode = 103
                MyResponse.ResponseMsg = "Product not exist"
            End If

            DataReader.Close()

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function GetOrderID(ByVal TM_AccountId As String, ByVal OMGProductID As Integer) As GetOrderIDRes

        '1.Select unifi portal order where order mode=5
        Dim MyResponse As New GetOrderIDRes

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("GetUnifiPortalProductSubscription", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountId
            cmd.Parameters.Add("@OMGProductID", SqlDbType.Int).Value = OMGProductID

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@OrderID", SqlDbType.VarChar, 50)
            cmd.Parameters("@OrderID").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.ResponseCode = cmd.Parameters("@Status").Value


            If MyResponse.ResponseCode = 1 Then
                MyResponse.OrderID = cmd.Parameters("@OrderID").Value.ToString()
                MyResponse.ResponseMsg = "Unifi Portal Order Exists"
            Else
                MyResponse.ResponseMsg = "Unifi Portal Order Not Exists"
            End If

        Catch ex As Exception
            ex.ToString()
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse

    End Function

    Function UpdateOTTOrderStatus(ByVal MyData As OTTOrderStatusRequestElement) As String


        '1.Get Unifi Portal Order
        Dim GetUnifiPortalOrder As New GetOrderIDRes
        Dim MyTMProduct As New TMProductDetails
        MyTMProduct = MyTMFunction.GetTMProductDetails("", MyData.productId, "")
        GetUnifiPortalOrder = GetOrderID(MyData.serviceId, MyTMProduct.OMGProductID)

        '2.If exists send request to unifi portal
        If GetUnifiPortalOrder.ResponseCode = 1 Then

            'JSON Seriallizer/Deseriallizer
            Dim js As New JavaScriptSerializer()

            Dim MyRequest As New OTTOrderStatusRequest
            Dim MyRequestElement As New OTTOrderStatusRequestElement
            Dim MyResponse As New OTTOrderStatusResponse
            Dim Status As New Status
            Dim JSONstrResponse As String = ""
            Dim TempDate As DateTime = DateTime.Now
            Dim Format As String = "dd-MM-yyyy hh24:mi:ss"
            Dim OrderDate As String = TempDate.ToString(Format)

            MyRequestElement.orderId = GetUnifiPortalOrder.OrderID
            MyRequestElement.orderStatus = MyData.orderStatus & "This is order status is from OMG"
            MyRequestElement.siebelOrderId = MyData.siebelOrderId
            MyRequestElement.orderType = MyData.orderType
            MyRequestElement.actionCode = MyData.actionCode
            MyRequestElement.productId = MyData.productId
            MyRequestElement.serviceId = MyData.serviceId
            MyRequestElement.dateTime = OrderDate
            MyRequest.OTTOrderStatusRequest = MyRequestElement

            'Sample JSON Format
            '{"OTTOrderStatusRequest": { "orderId" " 1UF18-N0000221","orderStatus": "SUCCESS","siebelOrderId": "1-27821905237",
            '"productId" "BBC", "serviceId": "abs@myunfi","dateTime": "27-02-2019 20:07:23"}}

            'Seriallizer JSON
            Dim JSONstrRequest As String = js.Serialize(MyRequest)

            'URL
            Dim UnifiPortalUrl As String = " "
            'Create a request using a URL that can receive a post. 
            Dim MyWebRequest As HttpWebRequest = WebRequest.Create(UnifiPortalUrl)
            ' Set the Method property of the request to POST.
            MyWebRequest.Method = "POST"
            ' Set the ContentType property of the WebRequest.
            MyWebRequest.ContentType = "application/json"

            ' Create POST data and convert it to a byte array.
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(JSONstrRequest)
            ' Set the ContentLength property of the WebRequest.
            MyWebRequest.ContentLength = byteArray.Length

            Try
                ' Get the request stream.
                Dim ReqDataStream As Stream = MyWebRequest.GetRequestStream()
                ' Write the data to the request stream.
                ReqDataStream.Write(byteArray, 0, byteArray.Length)
                ' Close the Stream object.
                ReqDataStream.Close()

                Dim MyWebResponse As HttpWebResponse = MyWebRequest.GetResponse()
                If MyWebResponse.StatusCode = HttpStatusCode.OK Then

                    ' MyWebResponse.ContentType = "application/json"
                    ' Get the stream containing content returned by the server.
                    Dim ResDataStream As Stream = MyWebResponse.GetResponseStream()
                    ' Open the stream using a StreamReader for easy access.
                    Dim Reader As New StreamReader(ResDataStream)
                    ' Read the content.
                    JSONstrResponse = Reader.ReadToEnd()
                    ' Display the content.
                    'Dim StrResponseResult As String = MyWebResponse.GetResponseHeader(ResponseValue)

                    'Sample JSON Response Format
                    '{"Status" {"Type": "OK", "Code": "0","Message": "ACKNOWLEDGED"}}

                    'JSON DeserializeObject
                    Dim dict As Object = js.DeserializeObject(JSONstrResponse)

                    For Each item As Object In dict
                        Status.Type = dict("Status")("Type").ToString
                        Status.Message = dict("Status")("Message").ToString
                        Status.Code = dict("Status")("Code").ToString
                    Next
                    MyResponse.Status = Status

                    ' Cleanup the streams and the response.
                    Reader.Close()
                    ResDataStream.Close()
                    MyWebResponse.Close()

                Else
                    MyResponse.ResponseCode = "-1"
                    MyResponse.ResponseMsg = "target URL is not reached"
                End If
            Catch ex As Exception
                MyResponse.ResponseCode = "-2"
                MyResponse.ResponseMsg = ex.Message

            End Try

            'APILog
            Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
            Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

            APILog.TM_AccountID = MyData.serviceId
            APILog.OMGProductID = MyTMProduct.OMGProductID
            APILog.EventName = "UpdateOTTOrderStatus"
            APILog.DataReq = DataToPost
            APILog.DataResp = MyResponseString
            APILog.ProcessStatus = ""
            MyOMGFunction.OMGAPILog(APILog)

        Else
            'Do Nothing

        End If

        Return GetUnifiPortalOrder.ResponseCode

    End Function

End Class
