﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Xml.Schema

'<XmlRootAttribute("rsp", IsNullable:=False)> _
'Public Class rsp_Login
'    <XmlAttribute()> _
'    Public status As String
'    Public err As err
'End Class

Public Class rsp
    <XmlAttribute()> _
    Public status As String
    Public err As err
    'login response
    Public sessionid As String
    'Get Message List
    ' Public msglist As msglist
    'sendmsg
    Public messageid As String
End Class

Public Class err
    <XmlAttribute()> _
    Public returncode As String
    <XmlAttribute()> _
    Public desc As String
End Class

'Public Class msglist
'    Public msginfo() As msginfo
'End Class

'Public Class msginfo
'    <XmlAttribute()> _
'    Public uid As String
'    <XmlAttribute()> _
'    Public status As String
'End Class

Public Class SendMsgObj
    Public sessionid As String
    Public msgtype As String
    Public message As String
    Public sendto As String
    Public filename As String
    Public transcid As String

End Class

Public Class INT_SendSMSReq
    Public TM_AccountID As String
    Public MobileNumber As String
    Public ProductId As String
    Public StrMsg As String
End Class

Public Class INT_SendSMSResp
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

Public Class InfoBlast_Object
    
End Class
