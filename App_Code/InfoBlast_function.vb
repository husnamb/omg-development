﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization

Imports System.Xml.Schema
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System

Public Class InfoBlast_function

    Sub WriteToLog(ByVal ErrorResponse As rsp)

        Dim ErrorData As String
        Dim LogName As String = "ErrorSendStatus_" & DateTime.Now.ToString("yyyyMMdd") & ".txt"

        'ErrorData = "MessageID =" & MsgID & "; returncode" & ErrorResponse.err.returncode & "; desc=" & ErrorResponse.err.desc
        ErrorData = "returncode" & ErrorResponse.err.returncode & "; desc=" & ErrorResponse.err.desc
        ' ErrorData = ErrorResponse.err.returncode

        Dim FILENAME As String = System.Web.HttpContext.Current.Server.MapPath("..\log\" & LogName)

        'Get a StreamWriter class that can be used to write to the file
        Dim objStreamWriter As StreamWriter
        objStreamWriter = File.AppendText(FILENAME)

        'Append the the end of the string, "A user viewed this demo at: "
        'followed by the current date and time
        objStreamWriter.WriteLine("Time : " & DateTime.Now.ToString())
        objStreamWriter.WriteLine("Error : " & ErrorData)

        'Close the stream
        objStreamWriter.Close()
    End Sub

    ' return the encryption in SHA1
    Function GetSHA1Encryption(ByVal toHash As String) As String

        Dim buffer As Byte() = Encoding.Default.GetBytes(toHash)
        Dim cryptoTransformSha1 As New SHA1CryptoServiceProvider()
        Dim hash As String = BitConverter.ToString(cryptoTransformSha1.ComputeHash(buffer)).Replace("-", "")
        'return to lower case
        Return hash.ToLower

    End Function

    Function HTTPPostTo(ByVal datatoPost As Object, ByVal URLtoPost As String) As String

        If URLtoPost <> Nothing Then

            Dim rspObj As New rsp
            Dim errObj As New err

            'Create a request using a URL that can receive a post. 
            Dim MyWebRequest As HttpWebRequest = WebRequest.Create(URLtoPost)
            ' Set the Method property of the request to POST.
            MyWebRequest.Method = "POST"
            ' Create POST data and convert it to a byte array.
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(datatoPost)
            ' Set the ContentType property of the WebRequest.
            MyWebRequest.ContentType = "application/x-www-form-urlencoded"
            ' Set the ContentLength property of the WebRequest.
            MyWebRequest.ContentLength = byteArray.Length

            Try
                ' Get the request stream.
                Dim dataStream As Stream = MyWebRequest.GetRequestStream()
                ' Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length)
                ' Close the Stream object.
                dataStream.Close()

                Dim MyWebResponse As HttpWebResponse = MyWebRequest.GetResponse()

                If MyWebResponse.StatusCode = "200" Then

                    'Now, we read the response (the string), and output it.
                    Dim MyResponse As Stream = MyWebResponse.GetResponseStream()
                    Dim MyResponseReader As StreamReader = New StreamReader(MyResponse)

                    'Read the response Stream and write it
                    Return MyResponseReader.ReadToEnd()

                    'Return datatoPost
                    MyResponseReader.Close()
                    MyWebResponse.Close()
                Else
                    Return "0"
                End If

            Catch ex As Exception
                'errObj.returncode = ex.StatusCode
                errObj.desc = ex.Message
                rspObj.err = errObj
                Call WriteToLog(rspObj)
                Return ex.ToString
            End Try
        Else
            Return "0"
        End If
    End Function

    Function GetSerializeXML(ByVal obj As Object, ByVal type As Type) As String

        Dim xmlDOc As New XmlDocument
        Dim sXML As String
        Dim xmlStream As New MemoryStream
        Dim xmlSerializer As XmlSerializer = New XmlSerializer(obj.GetType)
        Dim xmlTextWriter As XmlTextWriter = New XmlTextWriter(xmlStream, Encoding.UTF8)

        xmlSerializer.Serialize(xmlTextWriter, obj)
        xmlStream.Position = 0
        sXML = xmlStream.ToString
        xmlDOc.Load(xmlStream)

        Return xmlDOc.OuterXml

    End Function

    Function GetDeserializeXML(ByVal obj As String, ByVal type As Type) As Object

        Dim stringread As StringReader = New StringReader(obj)
        Dim xmlSerializer As XmlSerializer = New XmlSerializer(type)
        Dim xmlReader As XmlTextReader = New XmlTextReader(stringread)
        Dim anObject As New Object

        Try
            anObject = xmlSerializer.Deserialize(xmlReader)
        Catch ex As Exception

        Finally
            xmlReader.Close()
            stringread.Close()
        End Try

        Return anObject
    End Function

    'Function ConvertTimeToUnix() As String
    '    'unix time stamp since Jan 1st 1970 GMT
    '    Dim MyTime As DateTime = DateTime.UtcNow
    '    Dim uTime As Integer
    '    uTime = (MyTime - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds

    '    Return uTime.ToString

    'End Function

    'Function ConvertUnixToLocalTime(ByRef UnixTime As Integer) As String
    '    'unix time stamp since Jan 1st 1970 GMT
    '    Dim LocalTime As String
    '    LocalTime = New DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(UnixTime).ToLocalTime()

    '    Return LocalTime.ToString

    'End Function

    Function SMSInfoBlast(ByVal MyRequest As INT_SendSMSReq, Optional ByVal OrderNumber As String = "", Optional ByVal IVCode As String = "", Optional ByVal VCode As String = "") As INT_SendSMSResp

        Dim MyResponse As New INT_SendSMSResp

        Dim SessionID As String = ""
        Dim messageId As String = ""

        Dim ModTime As String = ""
        Dim txttranscid As String = ""
        Dim myError As String = ""

        If (Trim(MyRequest.MobileNumber) <> "") And (MyRequest.MobileNumber <> "NONE") And (Not MyRequest.MobileNumber Is Nothing) And (MyRequest.MobileNumber.Length <> 0) Then

            '1. Get sessionid
            Dim strUsername As String = "hypptv"
            Dim strPassword As String = "password"

            SessionID = System.Web.HttpUtility.HtmlEncode(LoginRequest(strUsername, strPassword))

            '2. Generate Transaction ID
            ModTime = DateTime.Now.ToString("yyyyMMdd")
            ModTime = ModTime & TimeOfDay.TimeOfDay.ToString("")
            ModTime = Replace(ModTime, " ", "")
            ModTime = Replace(ModTime, ":", "")
            txttranscid = ModTime

            If Trim(SessionID) <> "" Then

                '3. Send SMS
                Dim OBJ_SendMsg As New SendMsgObj

                OBJ_SendMsg.sessionid = SessionID
                OBJ_SendMsg.msgtype = "text"
                OBJ_SendMsg.message = "Hello World"
                OBJ_SendMsg.sendto = MyRequest.MobileNumber
                OBJ_SendMsg.filename = ""
                OBJ_SendMsg.transcid = txttranscid

                myError = SendMessage(OBJ_SendMsg, MyRequest.StrMsg)
                messageId = Trim(myError)

                If Trim(myError) <> "FAIL" Then
                    MyResponse.ResponseCode = 0
                    'MyResponse.ResponseMsg = "Error: " & messageId & "- Session ID: " & SessionID
                    'MyResponse.ResponseMsg = SessionID & " -- " & txttranscid
                    MyResponse.ResponseMsg = "Successful"
                Else
                    MyResponse.ResponseCode = 106
                    MyResponse.ResponseMsg = "Failed Transaction"
                End If
            Else
                MyResponse.ResponseCode = 101
                MyResponse.ResponseMsg = "Failed Transaction - Session ID"
            End If
        Else
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = "Mobile number is null"
        End If

        Dim MyDb As New dbConn
        Dim strConn As New SqlConnection(MyDb.connString)

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("SendSMSLog", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
            cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 50).Value = MyRequest.ProductId

            If (MyRequest.MobileNumber.Length <> 0) Then
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar, 20).Value = MyRequest.MobileNumber
            End If

            If Trim(OrderNumber) <> "" Then
                cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar, 50).Value = OrderNumber
            End If

            'Failed send verification code, revoke to be used again
            If Trim(IVCode) <> "" And Trim(VCode) <> "" And MyResponse.ResponseCode <> 0 Then
                cmd.Parameters.Add("@InitialVCode", SqlDbType.VarChar, 4).Value = IVCode
                cmd.Parameters.Add("@VCode", SqlDbType.VarChar, 6).Value = VCode
            End If

            If MyResponse.ResponseCode <> "101" Then
                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20).Value = UCase(Trim(myError))
                cmd.Parameters.Add("@MessageId", SqlDbType.VarChar, 50).Value = messageId
                cmd.Parameters.Add("@SessionID", SqlDbType.VarChar, 50).Value = SessionID
                cmd.Parameters.Add("@SMSTransactionID", SqlDbType.VarChar, 50).Value = txttranscid
                cmd.Parameters.Add("@SMSMsg", SqlDbType.VarChar, -1).Value = MyRequest.StrMsg
            End If

            cmd.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = MyResponse.ResponseMsg & " (" & MyResponse.ResponseCode & ")"

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function LoginRequest(ByVal username As String, ByVal password As String) As String

        Dim LoginURL As String = "http://www.infoblast.com.my/openapi/login.php"
        Dim dataToPost As String
        Dim strError As New err
        Dim MyResponse As String
        Dim PwdSha1 As String

        'convert to SHA 1
        PwdSha1 = GetSHA1Encryption(password)

        ' create the string for data to post
        dataToPost = "username=" & username & "&password=" & PwdSha1
        ' send to URL and retreived the response
        MyResponse = HTTPPostTo(dataToPost, LoginURL)

        Dim strResponse As New rsp

        strResponse = GetDeserializeXML(MyResponse, strResponse.GetType)

        If strResponse.status = "ok" Then
            Return strResponse.sessionid.ToString
            'Return MyResponse
        Else
            Return "fail"
        End If

        'Return MyResponse
    End Function

    'Function GetMessageList(ByVal sessionid As String, ByVal status As String) As String

    '    Dim LoginURL As String = "http://www.infoblast.com.my/openapi/getmsglist.php"
    '    Dim dataToPost As String
    '    Dim strError As New err
    '    Dim MyResponse As String

    '    ' create the string for data to post
    '    dataToPost = "sessionid=" & sessionid & "&status=" & status
    '    ' send to URL and retreived the response
    '    MyResponse = HTTPPostTo(dataToPost, LoginURL)

    '    'Dim strResponse As New rsp

    '    'strResponse = GetDeserializeXML(MyResponse, strResponse.GetType)

    '    'If strResponse.status = "OK" Then
    '    '    'Return strResponse.sessionid.ToString
    '    Return MyResponse
    '    'Else
    '    '    Return "FAIL"
    '    'End If

    '    Return MyResponse

    'End Function

    'Function to send message to recipient
    Function SendMessage(ByVal MsgObj As SendMsgObj, ByVal strMsg As String) As String

        Dim SendMsgURL As String = "http://www.infoblast.com.my/openapi/sendmsg.php"
        Dim dataToPost As String
        ' Dim strError As New err
        Dim MyResponse As String

        'strMsg = "RM0.00: TQ for subscribing to HyppTV. Your HyppFlicks Plus Voucher worth RM" & voucherValue & " code is " & VoucherNum & " and expired on " & ExpiryDate.ToString("dd/MM/yyyy") & ". TQ "
        'strMsg = "RM0.00: Here's a RM10 HyppFlicks Plus voucher for you to enjoy a movie on VOD. Your code is " & VoucherNum & " and expires on " & ExpiryDate.ToString("dd/MM/yyyy") & ". T&C apply."
        'strMsg = "RM0:Enjoy a HyppFlicks Plus movie (VOD) on us! Your code is " & VoucherNum & " and expires on " & ExpiryDate.ToString("dd/MM/yyyy") & ". Terms and Conditions apply."

        ' create the string for data to post
        dataToPost = "sessionid=" & MsgObj.sessionid
        dataToPost = dataToPost & "&msgtype=" & MsgObj.msgtype
        dataToPost = dataToPost & "&message=" & strMsg
        dataToPost = dataToPost & "&to=" & MsgObj.sendto
        dataToPost = dataToPost & "&filename=" & MsgObj.filename
        dataToPost = dataToPost & "&transcid=" & MsgObj.transcid

        ' send to URL and retreived the response
        MyResponse = HTTPPostTo(dataToPost, SendMsgURL)

        Dim strResponse As New rsp
        strResponse = GetDeserializeXML(MyResponse, strResponse.GetType)

        If strResponse.status = "OK" Then
            'Return "OK - " & strResponse.messageid.ToString
            Return strResponse.messageid.ToString
            'Return MyResponse
        Else
            Return "FAIL"
            'Return MyResponse
        End If

    End Function

    'Function to get send status
    Function GetSendStatus(ByVal sessionid As String, ByVal msgid As String) As String

        Dim SMSStatusURL As String = "http://www.infoblast.com.my/openapi/getsendstatus.php"
        Dim dataToPost As String
        Dim strError As New err
        Dim MyResponse As String

        ' create the string for data to post
        dataToPost = "sessionid=" & sessionid & "&msgid=" & msgid & "&bparty="
        ' send to URL and retreived the response
        MyResponse = HTTPPostTo(dataToPost, SMSStatusURL)

        Dim strResponse As New rsp

        strResponse = GetDeserializeXML(MyResponse, strResponse.GetType)

        If strResponse.status = "OK" Then
            Return MyResponse
        Else
            'Call WriteToLog(strResponse, msgid)
            Return MyResponse
        End If
    End Function

End Class
