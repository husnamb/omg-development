﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System

Public Class TMBilling_Function

    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)
    Dim MyOMGFunction As New OMG_Function

    'Testing Environment: ST4FC
    '=======================================
    'Dim NovaEAIValidate As String = "http://10.14.24.67:7001/prj_HsbbEai_Sync_War/httpMessageReceiver.do"
    'Dim NovaEAIValidate As String = "http://10.14.24.67:7001/prj_HsbbEai_Sync_War/ws/ValidateAccountId"
    'Dim ICPEAIValidate As String = "http://10.14.66.129:7001/iCareEAI_Sync_App_war/httpMessageReceiver.do"

    'Dim NOVAEAIOrder As String = "http://10.14.24.67:8001/prj_HsbbEai_Async_CRM_Inbound_War/httpMessageReceiver.do"
    'Dim NOVAEAIOrder As String = "http://10.14.24.67:8001/prj_HsbbEai_Async_CRM_Inbound_War/ws/SubmitOrderRequest"
    'Dim ICPEAIOrder As String = "http://10.14.66.129:8001/iCareEAI_Internal_Processing_war/httpMessageReceiver.do"

    'Testing Environment: UAT1/SIT10
    '=======================================
    Dim NovaEAIValidate As String = "http://10.14.28.219:7001/prj_HsbbEai_Sync_War/ws/ValidateAccountId"
    Dim ICPEAIValidate As String = "http://10.14.66.101:7001/iCareEAI_Sync_App_war/httpMessageReceiver.do"

    'Dim NOVAEAIOrder As String = "http://10.14.28.219:8001/prj_HsbbEai_Async_CRM_Inbound_War/httpMessageReceiver.do"
    Dim NOVAEAIOrder As String = "http://10.14.28.219:8001/prj_HsbbEai_Async_CRM_Inbound_War/ws/SubmitOrderRequest"
    Dim ICPEAIOrder As String = "http://10.14.66.101:8001/iCareEAI_Internal_Processing_war/httpMessageReceiver.do"

    Dim NOVAEPGStatus As String = "http://10.14.28.219:7001/prj_HsbbEai_Sync_War/ws/SubscriptionStatus/"

    'Testing Environment: PreProd/SIT11
    '=======================================
    'Dim NovaEAIValidate As String = "http://10.14.86.36:7280/prj_HsbbEai_Sync_War/ws/ValidateAccountId"
    'Dim ICPEAIValidate As String = "http://10.14.2.126:7001/iCareEAI_Sync_App_war/httpMessageReceiver.do"

    ''Dim NOVAEAIOrder As String = "http://10.14.86.36:7281/prj_HsbbEai_Async_CRM_Inbound_War/httpMessageReceiver.do"
    'Dim NOVAEAIOrder As String = "http://10.14.86.35:7281/prj_HsbbEai_Async_CRM_Inbound_War/ws/SubmitOrderRequest"
    'Dim ICPEAIOrder As String = "http://10.14.2.126:8001/iCareEAI_Internal_Processing_war/httpMessageReceiver.do"

    'Dim NOVAEPGStatus As String = "http://10.14.86.36:7280/prj_HsbbEai_Sync_War/ws/SubscriptionStatus/"

    'Production
    '=======================================
    'Dim NovaEAIValidate As String = "http://hpreaiwls-vip:7280/prj_HsbbEai_Sync_War/ws/ValidateAccountId"
    'Dim ICPEAIValidate As String = "http://npreaiwls-vip:7280/iCareEAI_Sync_App_war/httpMessageReceiver.do"

    'Dim NOVAEAIOrder As String = "http://hpreaiwli-vip:7281/prj_HsbbEai_Async_CRM_Inbound_War/ws/SubmitOrderRequest"
    'Dim ICPEAIOrder As String = "http://npreaiwli-vip:7281/iCareEAI_Internal_Processing_war/httpMessageReceiver.do"

    'Dim NOVAEPGStatus As String = "http://10.41.86.26:7280/prj_HsbbEai_Sync_War/ws/SubscriptionStatus/"

    'Description: To validate if the TM Account Id and Customer Id send by OMG is an existing TM Customer (for development purpose)
    Function TempValidateAccountId(ByVal MyRequest As INT_ValidateUserReq) As INT_ValidateUserResp

        Dim ValidateUserResp As New INT_ValidateUserResp()

        ValidateUserResp.ResponseCode = "0"
        ValidateUserResp.ResponseMsg = "Successful Validation"

        Select Case MyRequest.TM_AccountID

            Case "asnaida@unifi", "asnaida19@unifi", "nyum2@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "831020-08-5118"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "haidah.hussaini@tm.com.my"
                ValidateUserResp.TM_BillCycle = 16
                ValidateUserResp.AccountName = "Haidah Bt Hussaini"
                ValidateUserResp.City = "Ipoh"
                ValidateUserResp.StateName = "Perak"
                ValidateUserResp.BundleName = "VIP 5 (TM Staff)"
                ValidateUserResp.TM_AccountType = "Unifi"

            Case "husnamb@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "810101-01-2220"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "husna.baguri@tm.com.my"
                ValidateUserResp.TM_BillCycle = 18
                ValidateUserResp.AccountName = "Husna mAd Baguri"
                ValidateUserResp.City = "Puchong"
                ValidateUserResp.StateName = "Selangor"
                ValidateUserResp.BundleName = "VIP 10 (TM Staff)"
                ValidateUserResp.TM_AccountType = "Unifi"

            Case "mariza.azman@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "Passport"
                ValidateUserResp.Id = "E10504046"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "hareeshv@yupptv.com"
                ValidateUserResp.TM_BillCycle = 25
                ValidateUserResp.TM_AccountType = "Streamyx"

            Case "mie47@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "660125-14-7833"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "rohitreddyj@yupptv.com"
                ValidateUserResp.TM_BillCycle = 12
                ValidateUserResp.City = "Teluk Intan"
                ValidateUserResp.StateName = "Perak"
                ValidateUserResp.BundleName = "VIP 10"

            Case "ayeen79@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "Military"
                ValidateUserResp.Id = "T701851"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "narasimharaoy@yupptv.com"
                ValidateUserResp.TM_BillCycle = 4
                ValidateUserResp.BundleName = "VIP 10"

            Case "ybzahir@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "830225-08-5281"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "jayachandrak@yupptv.com"
                ValidateUserResp.TM_BillCycle = 25
                ValidateUserResp.TM_AccountType = "Streamyx"

            Case "sharulnizar203@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "810227-08-4553"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "harishm@yupptv.com"
                ValidateUserResp.TM_BillCycle = 17
                ValidateUserResp.City = "Shah Alam"
                ValidateUserResp.StateName = "Selangor"
                ValidateUserResp.BundleName = "VIP 10"

            Case "merryanne13@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "Passport"
                ValidateUserResp.Id = "E10504046"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "nithishk@yupptv.com"
                ValidateUserResp.TM_BillCycle = 25

            Case "niezan@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "760125-10-6262"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "anmishas@yupptv.com"
                ValidateUserResp.TM_BillCycle = 10
                ValidateUserResp.City = "Bangi"
                ValidateUserResp.StateName = "Selangor"
                ValidateUserResp.BundleName = "VIP 10 (TM Staff)"
                ValidateUserResp.TM_AccountType = "Unifi"

            Case "safianms@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "790110-14-5860"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "prashantha@yupptv.com"
                ValidateUserResp.TM_BillCycle = 19
                ValidateUserResp.City = "Johor Bahru"
                ValidateUserResp.StateName = "Johor"
                ValidateUserResp.BundleName = "Unifi Advanced Pro 30"

            Case "testlb_u1@unifi", "testlb_u2@unifi", "testlb_u3@unifi", "testlb_u4@unifi", "testlb_u5@unifi",
                "testvv_u1@unifi", "testvv_u2@unifi", "testvv_u3@unifi",
                "maziah", "erin",
                "testbbc_alp1@unifi", "testbbc_alp2@unifi", "testbbc_alp3@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "790110-14-5860"
                ValidateUserResp.MobileNumber = "0133337179"
                ValidateUserResp.CustomerEmail = "linlin@gmail.com"
                ValidateUserResp.TM_BillCycle = 19
                ValidateUserResp.City = "Johor Bahru"
                ValidateUserResp.StateName = "Johor"
                ValidateUserResp.BundleName = "Unifi Advanced Pro 30"

            Case "testifx_u1@unifi", "testifx_u2@unifi", "sharon1604@unifi", "girlly88@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "790110-14-5860"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "chris.brand@iflix.com"
                ValidateUserResp.TM_BillCycle = 19
                ValidateUserResp.City = "Johor Bahru"
                ValidateUserResp.StateName = "Johor"
                ValidateUserResp.BundleName = "Unifi Advanced Pro 30"

            Case "testifx_u3@unifi", "testifx_u4@unifi", "alqandra@unifi", "safianms@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "790110-14-5860"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "emmanuel.manyike@iflix.com"
                ValidateUserResp.TM_BillCycle = 19
                ValidateUserResp.City = "Johor Bahru"
                ValidateUserResp.StateName = "Johor"
                ValidateUserResp.BundleName = "Unifi Advanced Pro 30"

            Case "testifx_u1", "testifx_u2", "kmmattdm", "bgsay89"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "800719-11-5339"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "gavin@iflix.com"
                ValidateUserResp.TM_BillCycle = 19

            Case "testifx_u3", "testifx_u4", "din2607", "deanganu"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "800719-11-5339"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "tyrone.tudehope@iflix.com"
                ValidateUserResp.TM_BillCycle = 19

            Case "testlb_t1", "testlb_t2", "testlb_t3", "testlb_t4", "testlb_t5",
                    "testvv_t1", "testvv_t2", "testvv_t3",
                    "testyt_u1", "testyt_u2", "testyt_u3",
                    "testbbc_alp1", "testbbc_alp2", "testbbc_alp3"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "800719-11-5339"
                ValidateUserResp.MobileNumber = "0133420717"
                ValidateUserResp.CustomerEmail = "bobot@gmail.com"
                ValidateUserResp.TM_BillCycle = 19

            Case "khalil"
                ValidateUserResp.Status = "INACTIVE"
                ValidateUserResp.IdType = "Military"
                ValidateUserResp.Id = "T701851"
                ValidateUserResp.MobileNumber = "0133999261"
                ValidateUserResp.CustomerEmail = "wowwmena@gmail.com"
                ValidateUserResp.TM_BillCycle = 4
                ValidateUserResp.BundleName = "VIP 10"

            Case "lalita"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "Military"
                ValidateUserResp.Id = "T701851"
                ValidateUserResp.MobileNumber = ""
                ValidateUserResp.CustomerEmail = "mrsu69@gmail.com"
                ValidateUserResp.TM_BillCycle = 19
                ValidateUserResp.BundleName = "VIP 10"

            Case "niezan@unifi", "cikdet@unifi", "mena@unifi", "jooj@unifi", "mariana@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "760125-10-6262"
                ValidateUserResp.MobileNumber = "0193328318"
                ValidateUserResp.CustomerEmail = "niezan@tm.com.my"
                ValidateUserResp.TM_BillCycle = 10
                ValidateUserResp.City = "Bangi"
                ValidateUserResp.StateName = "Selangor"
                ValidateUserResp.BundleName = "VIP 10 (TM Staff)"
                ValidateUserResp.TM_AccountType = "Unifi"

            Case "jooj@unifi", "faizal"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "800118-10-6284"
                ValidateUserResp.MobileNumber = "0193328318"
                ValidateUserResp.CustomerEmail = "hasmah@tm.com.my"
                ValidateUserResp.TM_BillCycle = 18
                ValidateUserResp.City = "Bangi"
                ValidateUserResp.StateName = "Selangor"
                ValidateUserResp.BundleName = "Unifi Pro"

            Case "r70809@unifi", "elaine"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "820201-08-4803"
                ValidateUserResp.MobileNumber = "0123619559"
                ValidateUserResp.CustomerEmail = "mrsu69@gmail.com"
                ValidateUserResp.TM_BillCycle = 25
                ValidateUserResp.City = "Alor Gajah"
                ValidateUserResp.StateName = "Melaka"
                ValidateUserResp.BundleName = "VIP 20"

            Case "test22"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "New NRIC"
                ValidateUserResp.Id = "820201-08-4803"
                ValidateUserResp.MobileNumber = "0123619559"
                ValidateUserResp.CustomerEmail = "scopetestpro@gmail.com"
                ValidateUserResp.TM_BillCycle = 10

            Case "vas_newmedia@unifibiz"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "Internal Division ID"
                ValidateUserResp.Id = "TMV88887777"
                ValidateUserResp.MobileNumber = "0193328318"
                ValidateUserResp.CustomerEmail = "s51494@tm.com.my"
                ValidateUserResp.TM_BillCycle = 13

            Case "newmedia01@unifi"
                ValidateUserResp.Status = "ACTIVE"
                ValidateUserResp.IdType = "Internal Division ID"
                ValidateUserResp.Id = "NWM00000001"
                ValidateUserResp.MobileNumber = "0133944686"
                ValidateUserResp.CustomerEmail = "hafizuddin.shaharom@tm.com.my"
                ValidateUserResp.TM_BillCycle = 13

            Case Else
                ValidateUserResp.ResponseCode = "106"
                ValidateUserResp.ResponseMsg = "Failed Validation"
        End Select

        Return ValidateUserResp
    End Function

    'Description: To validate if the TM Account Id and Customer Id send by OMG is an existing TM Customer.
    Function ValidateAccountId(ByVal MyRequest As INT_ValidateUserReq) As INT_ValidateUserResp

        Dim MyResponse As New INT_ValidateUserResp
        Dim URL As String = ""
        Dim CheckAccountType As String = "NONE"
        Dim TempAccountType As String = "Unifi"
        Dim InsertDB As Boolean = False
        Dim RetryValidate As Boolean = False

        If MyRequest.TM_AccountType = "Unifi" Then

            'CheckAccountType = GetLocalAccount(MyRequest.TM_AccountID)

            If CheckAccountType = "Streamyx" Then
                URL = ICPEAIValidate
                TempAccountType = "Streamyx"
            Else
                URL = NovaEAIValidate
            End If
        ElseIf MyRequest.TM_AccountType = "Streamyx" Then
            URL = ICPEAIValidate
            TempAccountType = "Streamyx"
        End If

        MyResponse = ValidateAccountOrder(MyRequest.TM_AccountID, URL, TempAccountType)

        If MyRequest.TM_AccountType = "Unifi" Then

            Select Case CheckAccountType
                Case "NONE", "ERROR" 'Case 1: CheckAccountType = NONE / ERROR                
                    If MyResponse.ResponseCode = "0" Then
                        InsertDB = True
                    ElseIf MyResponse.ResponseCode = "106" Then
                        RetryValidate = True
                    End If
                Case "Unifi", "Streamyx"
                    'Case 2 : CheckAccountType = Unifi 
                    'Condition: 
                    '       1. Created in NOVA
                    '       2. User request for termination
                    '       3. Checking if account is created in ICP

                    'Case 3 : CheckAccountType = Streamyx 
                    'Condition: 
                    '       1. Created in ICP
                    '       2. User request for termination
                    '       3. Checking if account is created in NOVA
                    If MyResponse.ResponseCode = "106" Then
                        RetryValidate = True
                    End If
            End Select
        End If

        ''2nd Validation to ICP / NOVA platform
        If RetryValidate = True Then

            'If CheckAccountType = "Unifi" Then
            If TempAccountType = "Unifi" Then
                URL = ICPEAIValidate
                TempAccountType = "Streamyx"
            Else
                URL = NovaEAIValidate
                TempAccountType = "Unifi"
            End If
            MyResponse = ValidateAccountOrder(MyRequest.TM_AccountID, URL, TempAccountType)

            If TempAccountType = "Unifi" And MyResponse.ResponseCode = "0" Then
                InsertDB = True
            End If
            Return MyResponse
        Else
            Return MyResponse
        End If

        'MyResponse.ResponseMsg = CheckAccountType + " , " + InsertDB.ToString()

        'If InsertDB = True Then

        '    Try
        '        If strConn.State = ConnectionState.Closed Then
        '            strConn.Open()
        '        End If

        '        Dim cmd As New SqlCommand("InsertLocalAccount", strConn)

        '        cmd.CommandType = CommandType.StoredProcedure
        '        cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
        '        cmd.Parameters.Add("@Id", SqlDbType.VarChar, 50).Value = MyResponse.Id
        '        cmd.Parameters.Add("@IdType", SqlDbType.VarChar, 50).Value = MyResponse.IdType

        '        If TempAccountType = "Unifi" Then
        '            cmd.Parameters.Add("@PlatformType", SqlDbType.Int).Value = 1
        '        Else
        '            cmd.Parameters.Add("@PlatformType", SqlDbType.Int).Value = 2
        '        End If

        '        cmd.ExecuteNonQuery()

        '    Catch ex As Exception
        '        'Do nothing
        '    Finally
        '        If strConn.State = ConnectionState.Open Then
        '            strConn.Close()
        '        End If
        '    End Try
        'End If

        'If MyRequest.TM_AccountType = "Unifi" Then
        '    URL = NovaEAIValidate
        'ElseIf MyRequest.TM_AccountType = "Streamyx" Then
        '    URL = ICPEAIValidate
        'End If

    End Function

    Function ValidateAccountOrder(ByVal TM_AccountID As String, ByVal URL As String, ByVal AccountType As String) As INT_ValidateUserResp

        Dim MyResponse As New INT_ValidateUserResp

        '1. Request validation to NOVA / ICP
        Dim ValidateAccountReq As New ValidateAccountRequest
        Dim ValidateAccountResp As New ValidateAccountResponse

        ValidateAccountReq.EventName = "ValidateAccountId"
        ValidateAccountReq.TM_AccountID = TM_AccountID

        Dim Body As String = MyOMGFunction.GetSerializeXML(ValidateAccountReq, ValidateAccountReq.GetType)
        Dim DataToPost As String

        '2. Construct the XML format for NOVA user
        If AccountType = "Unifi" Then
            Body = Replace(Body, "<?xml version=""1.0"" encoding=""utf-8""?>", "")
            Body = Replace(Body, "<ValidateAccountRequest xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">", "<ValidateAccountRequest>")

            DataToPost = "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">"
            DataToPost = DataToPost & "<soapenv:Header/><soapenv:Body>"
            DataToPost = DataToPost & Body & "</soapenv:Body></soapenv:Envelope>"
        Else
            DataToPost = Body
        End If

        Dim MyResponseString As String = MyOMGFunction.HTTPPost(DataToPost, URL)
        Dim MyDataResponse As String = MyResponseString

        If AccountType = "Unifi" Then
            MyResponseString = MyOMGFunction.RemoveSoapEnvelope(MyResponseString, "<ValidateAccountResponse>")
        End If

        Try
            ValidateAccountResp = MyOMGFunction.GetDeserializeXML(MyResponseString, ValidateAccountResp.GetType)

            If ValidateAccountResp.ResponseCode = "0" Then

                If UCase(ValidateAccountResp.ResponseMsg) = "SUCCESS" Then

                    MyResponse.TMAccountID = ValidateAccountResp.TMAccountID
                    MyResponse.STBId = ValidateAccountResp.STBId

                    MyResponse.Status = ValidateAccountResp.Status
                    MyResponse.TM_AccountType = AccountType
                    MyResponse.MobileNumber = ValidateAccountResp.MobileNumber
                    MyResponse.CustomerEmail = ValidateAccountResp.CustomerEmail
                    MyResponse.TM_BillCycle = ValidateAccountResp.TM_BillCycle
                    MyResponse.Id = ValidateAccountResp.Id
                    MyResponse.IdType = ValidateAccountResp.IdType

                    MyResponse.AccountName = ValidateAccountResp.AccountName
                    MyResponse.HouseNo = ValidateAccountResp.HouseNo
                    MyResponse.FloorNo = ValidateAccountResp.FloorNo
                    MyResponse.BuildingName = ValidateAccountResp.BuildingName
                    MyResponse.StreetType = ValidateAccountResp.StreetType
                    MyResponse.StreetName = ValidateAccountResp.StreetName
                    MyResponse.Section = ValidateAccountResp.Section
                    MyResponse.PostalCode = ValidateAccountResp.PostalCode
                    MyResponse.City = ValidateAccountResp.City
                    MyResponse.StateName = ValidateAccountResp.StateName
                    MyResponse.Country = ValidateAccountResp.Country
                    MyResponse.HomePhone = ValidateAccountResp.HomePhone
                    MyResponse.BundleName = ValidateAccountResp.BundleName
                    MyResponse.ProductName = ValidateAccountResp.ProductName
                    MyResponse.AssetStatus = ValidateAccountResp.AssetStatus
                    MyResponse.ServiceNumber = ValidateAccountResp.ServiceNumber

                    MyResponse.BillingAccountNo = ValidateAccountResp.BillingAccountNo
                    MyResponse.OwnerAccountId = ValidateAccountResp.OwnerAccountId
                    MyResponse.Race = ValidateAccountResp.Race
                    MyResponse.ExchangeId = ValidateAccountResp.ExchangeId

                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Successful Transaction"

                ElseIf ValidateAccountResp.ResponseMsg = "Cannot create order. Account is not Active." _
                    Or ValidateAccountResp.ResponseMsg = "Cannot create order. Asset is not Active." _
                    Or ValidateAccountResp.ResponseMsg = "Cannot create order. Asset is not Active" Then
                    MyResponse.ResponseCode = 103
                    MyResponse.ResponseMsg = "Account is NOT active"

                ElseIf ValidateAccountResp.ResponseMsg = "Cannot create order. Asset is not Exist." Then
                    MyResponse.ResponseCode = 106
                    MyResponse.ResponseMsg = "Account NOT Exist"
                Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = ValidateAccountResp.ResponseMsg
                End If
            Else
                MyResponse.ResponseCode = 101
                MyResponse.ResponseMsg = ValidateAccountResp.ResponseMsg
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = ex.ToString
        End Try

        Dim APILog As New INT_APILog
        APILog.TM_AccountID = TM_AccountID
        APILog.EventName = "ValidateAccountId"
        APILog.EventURL = URL
        APILog.DataReq = DataToPost
        APILog.DataResp = MyDataResponse

        If MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If

        TMBillingAPILog(APILog)

        Return MyResponse
    End Function

    Function SetOMGTransactionID(ByVal MyRequest As INT_SetTransactionIDReq) As INT_SetTransactionIDResp

        Dim MyResponse As New INT_SetTransactionIDResp

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("GenerateTxnId", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
            cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar, 20).Value = MyRequest.MobileNumber
            cmd.Parameters.Add("@CustomerEmail", SqlDbType.VarChar, 50).Value = MyRequest.CustomerEmail
            cmd.Parameters.Add("@Id", SqlDbType.VarChar, 50).Value = MyRequest.Id
            cmd.Parameters.Add("@IdType", SqlDbType.VarChar, 50).Value = MyRequest.IdType
            cmd.Parameters.Add("@CycleDate", SqlDbType.VarChar, 10).Value = MyRequest.TM_BillCycle
            cmd.Parameters.Add("@OTT_TXNID", SqlDbType.VarChar, 15).Value = MyRequest.OTT_TXNID
            cmd.Parameters.Add("@Token", SqlDbType.VarChar, -1).Value = MyRequest.Token

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@OMG_TXNID", SqlDbType.VarChar, 15)
            cmd.Parameters("@OMG_TXNID").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()

            If MyResponse.Status = "1" Then
                MyResponse.OMG_TXNID = cmd.Parameters("@OMG_TXNID").Value.ToString()
            End If

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function GetOMGTransactionID(ByVal MyRequest As INT_GetTransactionIDReq) As INT_GetTransactionIDResp

        Dim MyResponse As New INT_GetTransactionIDResp

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim oRSItem As SqlDataReader
            Dim cmd As New SqlCommand("VerifyTxnId", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@OMG_TXNID", SqlDbType.VarChar, 15).Value = MyRequest.OMG_TXNID
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID

            oRSItem = cmd.ExecuteReader

            If oRSItem.HasRows Then

                oRSItem.Read()

                If oRSItem("Status") = "VALID" Then
                    MyResponse.OMG_TXNID = oRSItem("OMG_TXNID")
                    MyResponse.TM_AccountID = oRSItem("TM_AccountID")
                    MyResponse.MobileNumber = oRSItem("MobileNumber")
                    MyResponse.CustomerEmail = oRSItem("CustomerEmail")
                    MyResponse.TM_BillCycle = oRSItem("TM_BillCycle")
                    MyResponse.OTT_ID = oRSItem("OTT_ID")
                    MyResponse.OTT_TXNID = oRSItem("OTT_TXNID")
                    MyResponse.ProductID = oRSItem("ProductID")
                    MyResponse.Status = oRSItem("Status")

                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Successful Transaction"
                Else
                    MyResponse.ResponseCode = 3
                    MyResponse.ResponseMsg = "Invalid transaction ID"
                End If
            Else
                MyResponse.ResponseCode = 1
                MyResponse.ResponseMsg = "Data NOT exist"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function BillingValidation(ByVal TM_AccountID As String, ByVal TMProductID As Integer) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("GetBillingValidation", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = TMProductID

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@CycleDate", SqlDbType.VarChar, 10)
            cmd.Parameters("@CycleDate").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@OSMActivationDate", SqlDbType.VarChar, 20)
            cmd.Parameters("@OSMActivationDate").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()

            If cmd.Parameters("@CycleDate").Value.ToString <> "" Then
                MyResponse.TM_BillCycle = cmd.Parameters("@CycleDate").Value.ToString()
            Else
                MyResponse.TM_BillCycle = "0"
            End If

            If cmd.Parameters("@OSMActivationDate").Value.ToString <> "" Then
                MyResponse.OSMActivationDate = cmd.Parameters("@OSMActivationDate").Value.ToString()
            Else
                MyResponse.OSMActivationDate = "0"
            End If

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function BillingSubscription(ByVal MyRequest As OMG_SubmitOrderRequest, ByVal TMProductID As Integer, ByVal MerchantID As String, Optional ByVal ReqType As String = "TM", Optional ByVal CycleDate As String = "") As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("BillingSubscription", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
            cmd.Parameters.Add("@TMAccountType", SqlDbType.VarChar, 10).Value = MyRequest.TM_AccountType
            cmd.Parameters.Add("@TMMobileNo", SqlDbType.VarChar, 20).Value = MyRequest.TM_MobileNo
            cmd.Parameters.Add("@TMEmail", SqlDbType.VarChar, 256).Value = MyRequest.TM_Email
            cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = TMProductID
            cmd.Parameters.Add("@MerchantID", SqlDbType.VarChar, 10).Value = MerchantID
            cmd.Parameters.Add("@ReqType", SqlDbType.VarChar, 10).Value = ReqType

            If ReqType = "TM" Then
                cmd.Parameters.Add("@OSMActivationDate", SqlDbType.VarChar, 20).Value = MyRequest.ActivationDate
            Else
                cmd.Parameters.Add("@CycleDate", SqlDbType.VarChar, 10).Value = CycleDate
            End If

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@billID", SqlDbType.Int)
            cmd.Parameters("@billID").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.TMBillID = cmd.Parameters("@billID").Value
            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    'Description: To create OTT order in NOVA SIEBEL.
    Function NewOTTOrder(ByVal MyRequest As INT_NewOrderReq) As INT_NewOrderResp

        Dim MyResponse As New INT_NewOrderResp
        Dim URL As String = ""

        If MyRequest.TM_AccountType = "Unifi" Then
            URL = NOVAEAIOrder
        ElseIf MyRequest.TM_AccountType = "Streamyx" Then
            URL = ICPEAIOrder
        End If

        '2. Request OrderReq to NOVA / ICP
        Dim OrderReq As New SubmitOrderRequest
        Dim OrderResp As New SubmitOrderRequest

        OrderReq.EventName = "NewOTTOrder"
        OrderReq.TM_AccountID = MyRequest.TM_AccountID
        OrderReq.ProductId = MyRequest.TMProductCode
        OrderReq.ProductName = MyRequest.TMProductName

        If MyRequest.ReqType = "Order" Then
            OrderReq.ActivationDate = MyRequest.ActivationDate

            If MyRequest.ProductType = "OTC" Then
                OrderReq.OTCIndicator = "Y"
            Else
                OrderReq.OTCIndicator = "N"
            End If
            OrderReq.RemoveOTTFlag = "N"

        ElseIf MyRequest.ReqType = "Terminate" Then
            OrderReq.DeactivationDate = MyRequest.DeactivationDate
            OrderReq.OTCIndicator = "N"
            OrderReq.RemoveOTTFlag = "Y"
        End If

        Dim Body As String = MyOMGFunction.GetSerializeXML(OrderReq, OrderReq.GetType)
        Dim DataToPost As String

        '2. Construct the XML format for NOVA user
        If MyRequest.TM_AccountType = "Unifi" Then
            Body = Replace(Body, "<?xml version=""1.0"" encoding=""utf-8""?>", "")
            Body = Replace(Body, "<SubmitOrderRequest xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">", "<SubmitOrderRequest>")

            DataToPost = "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">"
            DataToPost = DataToPost & "<soapenv:Header/><soapenv:Body>"
            DataToPost = DataToPost & Body & "</soapenv:Body></soapenv:Envelope>"
        Else
            DataToPost = Body
        End If

        Dim MyResponseString As String = MyOMGFunction.HTTPPost(DataToPost, URL)
        Dim MyDataResponse As String = MyResponseString

        If MyRequest.TM_AccountType = "Unifi" Then
            MyResponseString = MyOMGFunction.RemoveSoapEnvelope(MyResponseString, "<SubmitOrderRequest>")
        End If

        Try
            OrderResp = MyOMGFunction.GetDeserializeXML(MyResponseString, OrderResp.GetType)

            If OrderResp.EventName = OrderReq.EventName And
                OrderResp.TM_AccountID = OrderReq.TM_AccountID And
                OrderResp.ProductId = OrderReq.ProductId And
                OrderResp.ProductName = OrderReq.ProductName And
                OrderResp.OTCIndicator = OrderReq.OTCIndicator And
                OrderResp.RemoveOTTFlag = OrderReq.RemoveOTTFlag Then

                If MyRequest.ReqType = "Order" Then

                    If OrderResp.ActivationDate = OrderReq.ActivationDate Then
                        MyResponse.ResponseCode = 0
                        MyResponse.ResponseMsg = "Successful transaction"
                    Else
                        MyResponse.ResponseCode = 107
                        MyResponse.ResponseMsg = "Unsuccessful order request (Order)"
                    End If
                ElseIf MyRequest.ReqType = "Terminate" Then

                    If OrderResp.DeactivationDate = OrderReq.DeactivationDate Then
                        MyResponse.ResponseCode = 0
                        MyResponse.ResponseMsg = "Successful transaction"
                    Else
                        MyResponse.ResponseCode = 107
                        MyResponse.ResponseMsg = "Unsuccessful order request (Terminate)"
                    End If
                End If
            Else
                MyResponse.ResponseCode = 101
                MyResponse.ResponseMsg = "Technical error - NewOTTOrder"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = ex.ToString
        End Try

        Dim APILog As New INT_APILog
        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.TMProductID = MyRequest.TMProductID
        APILog.EventName = "NewOTTOrder"
        APILog.EventURL = URL
        APILog.DataReq = DataToPost
        APILog.DataResp = MyDataResponse

        If MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        TMBillingAPILog(APILog)

        Return MyResponse
    End Function

    Function Update_NewOTTOrder(ByVal TM_AccountID As String, ByVal TMProductID As Integer, Optional ByVal OrderNumber As String = "") As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("UpdateOrderResponse", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = TMProductID

            If Trim(OrderNumber) <> "" Then
                cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar, 30).Value = OrderNumber
            End If

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@ErrorMsg", SqlDbType.VarChar, 50)
            cmd.Parameters("@ErrorMsg").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar, 20)
            cmd.Parameters("@MobileNumber").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()
            MyResponse.MobileNumber = cmd.Parameters("@MobileNumber").Value.ToString()

            If MyResponse.Status <> "1" Then
                MyResponse.ErrorMsg = cmd.Parameters("@ErrorMsg").Value.ToString()
            End If
            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function BillingTermination(ByVal TM_AccountID As String, ByVal TMProductID As Integer, ByVal TerminateStatus As String, Optional ByVal BillCycleDate As String = "") As PostResponse

        'TERMINATE ONLY

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("BillingTermination", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = TMProductID
            cmd.Parameters.Add("@TerminateStatus", SqlDbType.VarChar, 30).Value = TerminateStatus

            If Trim(BillCycleDate) <> "" Then
                cmd.Parameters.Add("@CycleDate", SqlDbType.VarChar, 10).Value = BillCycleDate
            End If

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output
            cmd.Parameters.Add("@TerminationDate", SqlDbType.VarChar, 50)
            cmd.Parameters("@TerminationDate").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()
            If MyResponse.Status = 1 Then
                MyResponse.TerminationDate = cmd.Parameters("@TerminationDate").Value
            End If
            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    '============================================================================================================================
    'General function
    '============================================================================================================================

    Function GetTMProductDetails(ByVal MerchantID As String, ByVal ProductID As String, Optional ByVal PlatformType As String = "") As TMProductDetails

        Dim MyProductDetails As New TMProductDetails

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            'Create reader
            Dim oRSItem As SqlDataReader
            Dim cmd As New SqlCommand("GetTMProductDetails", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            If Trim(MerchantID) <> "" Then
                cmd.Parameters.Add("@merchantID", SqlDbType.VarChar, 10).Value = MerchantID
            End If
            cmd.Parameters.Add("@ProductID", SqlDbType.VarChar, 50).Value = ProductID

            If Trim(PlatformType) <> "" Then
                cmd.Parameters.Add("@ReqType", SqlDbType.VarChar, 10).Value = "OMG"
                cmd.Parameters.Add("@PlatformType", SqlDbType.VarChar, 10).Value = PlatformType
            Else
                cmd.Parameters.Add("@ReqType", SqlDbType.VarChar, 10).Value = "TM"
            End If

            oRSItem = cmd.ExecuteReader

            If oRSItem.HasRows Then
                oRSItem.Read()
                MyProductDetails.TMMerchantID = oRSItem("TMMerchantID")
                MyProductDetails.TMProductID = oRSItem("tm_id")
                MyProductDetails.TMProductCode = oRSItem("tm_productCode")
                MyProductDetails.TMProductName = oRSItem("tm_productName")
                MyProductDetails.ProductType = oRSItem("typeDesc")
                MyProductDetails.OMGProductID = oRSItem("omg_id")
                MyProductDetails.OMGProductCode = oRSItem("omg_productCode")
                MyProductDetails.OMGProductName = oRSItem("omg_productName")
                MyProductDetails.OTTMerchantID = oRSItem("OTTMerchantID")
                MyProductDetails.Status = oRSItem("statusdesc")

                MyProductDetails.ResponseCode = 0
                MyProductDetails.ResponseMsg = "Successful Transaction"
            Else
                MyProductDetails.ResponseCode = 1
                MyProductDetails.ResponseMsg = "Product ID not exist"
            End If

        Catch ex As Exception
            MyProductDetails.ResponseCode = 2
            MyProductDetails.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyProductDetails
    End Function

    'Description: To log every request and response to NOVA/ICP system.
    Function TMBillingAPILog(ByVal MyRequest As INT_APILog) As Integer

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("BillingAPILog", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID

            If Trim(MyRequest.TMProductID) <> "" Then
                cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = MyRequest.TMProductID
            End If

            cmd.Parameters.Add("@EventName", SqlDbType.VarChar, 30).Value = MyRequest.EventName

            If Trim(MyRequest.EventURL) <> "" Then
                cmd.Parameters.Add("@EventURL", SqlDbType.VarChar, -1).Value = MyRequest.EventURL
            End If

            If Trim(MyRequest.DataReq) <> "" Then
                cmd.Parameters.Add("@DataReq", SqlDbType.VarChar, -1).Value = MyRequest.DataReq
            End If
            If Trim(MyRequest.DataResp) <> "" Then
                cmd.Parameters.Add("@DataResp", SqlDbType.VarChar, -1).Value = MyRequest.DataResp
            End If

            If Trim(MyRequest.ProcessStatus) <> "" Then
                cmd.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = MyRequest.ProcessStatus
            End If

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            'Do nothing
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

    End Function

    Function GetLocalAccount(ByVal TM_AccountID As String) As String

        Dim AccountType As String = "NONE"

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("CheckLocalAccount", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@PlatformType", SqlDbType.VarChar, 20)
            cmd.Parameters("@PlatformType").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            AccountType = cmd.Parameters("@PlatformType").Value.ToString()

            If AccountType = "" Then
                AccountType = "NONE"
            End If

        Catch ex As Exception
            AccountType = "ERROR"
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return AccountType
    End Function

    '============================================================================================================================
    'FOR INTERNAL USE
    '============================================================================================================================
    Function GetSubscriptionStatus(ByVal MyRequest As INT_SubscriptionStatusReq) As OMG_GetProductStatusResp

        Dim MyResponse As New OMG_GetProductStatusResp

        Dim StatusReq As New SubscriptionStatusRequest
        Dim StatusResp As New SubscriptionStatusResponse

        Dim ErrorMessage As String = ""
        Dim DataToPost As String = ""
        Dim MyResponseString As String = ""
        Dim MyDataResponse As String = ""

        Try
            If MyRequest.ProductList.Total > 0 Then

                StatusReq.EventName = "EPGSubscriptionStatus"
                StatusReq.ServiceID = MyRequest.SubscriberID

                Dim x As Integer = 0

                Dim ReqProductList As New ProductList
                Dim ReqProduct(MyRequest.ProductList.Total) As Product

                For x = 0 To (MyRequest.ProductList.Total - 1)
                    Dim Product_x As New Product()
                    Product_x.Id = MyRequest.ProductList.Product(x).Id
                    ReqProduct(x) = Product_x
                Next

                ReqProductList.Product = ReqProduct
                ReqProductList.Total = MyRequest.ProductList.Total
                StatusReq.ProductList = ReqProductList

                Dim Body As String = MyOMGFunction.GetSerializeXML(StatusReq, StatusReq.GetType)

                Body = Replace(Body, "<?xml version=""1.0"" encoding=""utf-8""?>", "")
                Body = Replace(Body, "<SubscriptionStatusRequest xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">", "<SubscriptionStatusRequest>")

                DataToPost = "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">"
                DataToPost = DataToPost & "<soapenv:Header/><soapenv:Body>"
                DataToPost = DataToPost & Body & "</soapenv:Body></soapenv:Envelope>"

                MyResponseString = MyOMGFunction.HTTPPost(DataToPost, NOVAEPGStatus)
                MyDataResponse = MyOMGFunction.RemoveSoapEnvelope(MyResponseString, "<SubscriptionStatusResponse>")

                StatusResp = MyOMGFunction.GetDeserializeXML(MyDataResponse, StatusResp.GetType)

                If StatusResp.EventName = "EPGSubscriptionStatus" Then

                    If StatusResp.ResponseCode = "0" Then

                        MyResponse.SubscriberID = StatusResp.ServiceID

                        Try
                            'Counting XML nodes
                            Dim doc As XmlDocument = New XmlDocument()
                            doc.LoadXml(MyDataResponse)

                            Dim root As XmlElement = doc.DocumentElement
                            Dim elemList As XmlNodeList = root.GetElementsByTagName("Product")

                            If elemList.Count > 0 Then

                                Dim y As Integer = 0
                                Dim RespProductList As New ProductList
                                Dim RespProduct(MyRequest.ProductList.Total) As Product

                                For y = 0 To (elemList.Count - 1)
                                    Dim Product_y As New Product()
                                    Product_y.Id = StatusResp.ProductList.Product(y).Id
                                    Product_y.Status = StatusResp.ProductList.Product(y).Status
                                    RespProduct(y) = Product_y
                                Next

                                RespProductList.Product = RespProduct
                                RespProductList.Total = elemList.Count
                                MyResponse.ProductList = RespProductList

                                MyResponse.ResponseCode = 0
                                MyResponse.ResponseMsg = "Successful Transaction"
                            Else
                                MyResponse.ResponseCode = 102
                                MyResponse.ResponseMsg = "Siebel return zero"
                            End If
                        Catch ex As Exception
                            MyResponse.ResponseCode = 101
                            MyResponse.ResponseMsg = "Technical Error"
                            ErrorMessage = "Error Deserialize XML Siebel"
                        End Try

                    ElseIf StatusResp.ResponseCode = "E01" Then
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid Service ID"

                    ElseIf StatusResp.ResponseCode = "E02" Then
                        MyResponse.ResponseCode = 103
                        MyResponse.ResponseMsg = "Product list empty (E02)"
                    Else
                        MyResponse.ResponseCode = 101
                        MyResponse.ResponseMsg = "Technical Error"
                        ErrorMessage = StatusResp.ResponseCode & " - " & StatusResp.ResponseMsg
                    End If
                Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical Error"
                    ErrorMessage = "Incorrect EventName return"
                End If
            Else
                MyResponse.ResponseCode = 103
                MyResponse.ResponseMsg = "Product list empty"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 101
            MyResponse.ResponseMsg = "Technical Error - Request to Siebel"
            ErrorMessage = MyResponseString
        End Try

        Dim APILog As New INT_APILog
        APILog.TM_AccountID = MyRequest.SubscriberID
        APILog.EventName = "EPGSubscriptionStatus"
        APILog.EventURL = NOVAEPGStatus
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If (MyResponse.ResponseCode = "101") Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If

        TMBillingAPILog(APILog)

        Return MyResponse
    End Function

    '============================================================================================================================================
    'TO USE WHEN RECEIVE ERROR FROM SIEBEL
    '============================================================================================================================================
    Function GetOTTOrder(ByVal MyRequest As INT_OTTOrderReq) As INT_OTTOrderResp

        Dim MyResponse As New INT_OTTOrderResp

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim oRSItem As SqlDataReader
            Dim cmd As New SqlCommand("GetOTTOrder", strConn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@TMBill_ID", SqlDbType.Int).Value = MyRequest.TMBill_ID
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID

            oRSItem = cmd.ExecuteReader

            If oRSItem.HasRows Then

                oRSItem.Read()

                If oRSItem("statusdesc") = "PROCESSING" Then

                    MyResponse.TM_AccountID = oRSItem("TM_AccountID")
                    MyResponse.TM_AccountType = oRSItem("TM_AccountType")
                    MyResponse.TM_MobileNo = oRSItem("TM_MobileNo")
                    MyResponse.TM_Email = oRSItem("TM_Email")
                    MyResponse.TM_BillCycle = oRSItem("TM_BillingCycleDate")
                    MyResponse.Status = oRSItem("statusdesc")
                    MyResponse.ActivationDate = oRSItem("transaction_date")
                    MyResponse.TMProductID = oRSItem("tm_id")
                    MyResponse.TMProductCode = oRSItem("tm_productCode")
                    MyResponse.TMProductName = oRSItem("productName")
                    MyResponse.ProductType = oRSItem("typeDesc")

                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Successful Transaction"
                Else
                    MyResponse.ResponseCode = 1
                    MyResponse.ResponseMsg = "Subscription is not available"
                End If
            Else
                MyResponse.ResponseCode = 1
                MyResponse.ResponseMsg = "No Data"
            End If

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    Function BillingSubscriptionRetry(ByVal TM_AccountID As String, ByVal TMProductID As Integer, ByVal TMBill_ID As Integer, ByVal RetryType As String) As PostResponse

        Dim MyResponse As New PostResponse

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("BillingSubscriptionRetry", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = TM_AccountID
            cmd.Parameters.Add("@TMProductID", SqlDbType.Int).Value = TMProductID
            cmd.Parameters.Add("@BillID", SqlDbType.Int).Value = TMBill_ID
            cmd.Parameters.Add("@RetryType", SqlDbType.VarChar, 10).Value = RetryType

            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 10)
            cmd.Parameters("@Status").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            MyResponse.Status = cmd.Parameters("@Status").Value.ToString()

            MyResponse.ResponseCode = 0
            MyResponse.ResponseMsg = "Successful Transaction"

        Catch ex As Exception
            MyResponse.ResponseCode = 2
            MyResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return MyResponse
    End Function

    '============================================================================================================================
    'For Teka Tekan
    '============================================================================================================================
    Sub BulkValidateAccountId(ByVal MyAccountIDs() As AccountID, ByVal ShowID As Integer)

        If MyAccountIDs.Length > 0 Then

            Dim FileDate As String = DateTime.Now.ToString("yyyyMMdd")
            Dim Filename As String = ""
            Dim LocalSourcePath As String = "E:\www\OMG_prod\GameShowArchiving\new\"
            Dim LocalMovePath As String = "E:\www\OMG_prod\GameShowArchiving\sent\"
            Dim LocalLogPath As String = "E:\www\OMG_prod\GameShowArchiving\log\"
            Dim CountTotal As Integer = 0
            Dim AccountWithError As String = ""

            If ShowID <> 0 Then
                Filename = "OMG_" & ShowID & "_" & FileDate & ".txt"
            Else
                Filename = "rand_" & FileDate & ".csv"
                LocalSourcePath = LocalSourcePath.Replace("GameShowArchiving", "BulkAccountArchiving")
                LocalLogPath = LocalLogPath.Replace("GameShowArchiving", "BulkAccountArchiving")
            End If

            Dim FileExists As Boolean = False
            Dim FilePath As String = String.Format(LocalSourcePath & "{0}", Filename)

            If ShowID = 0 Then
                FileExists = File.Exists(FilePath)
                If Not FileExists Then
                    LogFile(FilePath, "Account Id,Fullname,House No,Building Name,Street Type,Street Name,Section,Postal Code,City,State Name")
                    FileExists = False
                End If
            End If

            Dim x As Integer = 0

            'Check TM_AccountID
            Dim GetAccountResp As New AccountDetails

            Dim ValidateUserReq As New INT_ValidateUserReq
            Dim ValidateUserResp As New INT_ValidateUserResp

            Dim FullName As String = ""
            Dim Id As String = ""
            Dim IdType As String = ""
            Dim FullAddress As String = ""
            Dim Area As String = ""
            Dim StaffFlag As Integer = 0
            Dim MobileNumber As String = ""
            Dim Email As String = ""

            Dim FileInput As String = ""

            For x = 0 To (MyAccountIDs.Length - 1)

                FileInput = ""
                FullAddress = ""

                GetAccountResp = MyOMGFunction.GetAccount(MyAccountIDs(x).TM_AccountID)

                If GetAccountResp.ServiceID <> "-" Then

                    'Request account validation to NOVA / ICP
                    ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
                    ValidateUserReq.TM_AccountType = GetAccountResp.AccountType
                    ValidateUserResp = ValidateAccountId(ValidateUserReq)

                    If ShowID <> 0 Then

                        If ValidateUserResp.ResponseCode = "0" And UCase(ValidateUserResp.Status) = "ACTIVE" Then

                            If ValidateUserResp.AccountName IsNot Nothing Then
                                FullName = ValidateUserResp.AccountName
                            Else
                                FullName = ""
                            End If

                            If ValidateUserResp.IdType IsNot Nothing Then
                                IdType = ValidateUserResp.IdType
                            Else
                                IdType = ""
                            End If

                            If ValidateUserResp.Id IsNot Nothing Then
                                Id = ValidateUserResp.Id
                            Else
                                Id = ""
                            End If

                            If (ValidateUserResp.HouseNo IsNot Nothing) And (Trim(ValidateUserResp.HouseNo) <> "") Then
                                FullAddress = ValidateUserResp.HouseNo & ","
                            End If

                            If (ValidateUserResp.BuildingName IsNot Nothing) And (Trim(ValidateUserResp.BuildingName) <> "") Then
                                FullAddress += ValidateUserResp.BuildingName & ","
                            End If

                            If (ValidateUserResp.StreetType IsNot Nothing) And (Trim(ValidateUserResp.StreetType) <> "") Then
                                FullAddress += ValidateUserResp.StreetType
                            End If

                            If (ValidateUserResp.StreetName IsNot Nothing) And (Trim(ValidateUserResp.StreetName) <> "") Then
                                FullAddress += " " & ValidateUserResp.StreetName & ","
                            End If

                            If (ValidateUserResp.Section IsNot Nothing) And (Trim(ValidateUserResp.Section) <> "") Then
                                FullAddress += ValidateUserResp.Section & ","
                            End If

                            If (ValidateUserResp.PostalCode IsNot Nothing) And (Trim(ValidateUserResp.PostalCode) <> "") Then
                                FullAddress += ValidateUserResp.PostalCode & " "
                            End If

                            If ValidateUserResp.City IsNot Nothing And ValidateUserResp.StateName IsNot Nothing Then
                                Area = ValidateUserResp.City & "," & ValidateUserResp.StateName
                            Else
                                Area = ""
                            End If
                            FullAddress += Area

                            Select Case ValidateUserResp.BundleName
                                Case "Streamyx Staff Combo Mobility 384k (Bundle)", "Stx Staff Combo Mobility 384k (Bundle)",
                                        "Streamyx Staff Combo Mobility 1.0M (Bundle)", "Streamyx Staff Combo Mobility 2.0M (Bundle)",
                                        "Streamyx Staff Combo Mobility 4.0M (Bundle)", "Streamyx Staff Combo Mobility 512k (Bundle)",
                                        "Stx Staff Combo Mobility 1.0M (Bundle) Free Speed Upgrade (BIP)", "Stx Staff Combo Mobility 2.0M (Bundle) Free Speed Upgrade (BIP)",
                                        "Stx Staff Combo Mobility 4.0M (Bundle) Free Speed Upgrade (BIP)", "Stx Staff Combo Mobility 8.0M (HyppTV) (Bundle) Free Speed Upgrade (BIP)",
                                        "Stx Staff Combo Mobility 8.0M (HyppTV)(Bundle)", "Streamyx Staff Combo Mobility 8.0M (HyppTV)(Bundle)",
                                        "Stx Staff Combo Mobility 1.0M (Bundle)", "Stx Staff Combo Mobility 2.0M (Bundle)", "Stx Staff Combo Mobility 4.0M (Bundle)",
                                        "Stx Staff Combo Mobility 512k (Bundle)", "Streamyx StaffPack 2.0M (BBPC) (Bundle)", "Streamyx StaffPack 4.0M (BBPC) (Bundle)",
                                        "Streamyx StaffPack (home prepaid) 1.0M (BBPC) (Bundle)", "Streamyx StaffPack (home prepaid) 2.0M (BBPC) (Bundle)",
                                        "Streamyx StaffPack (home prepaid) 4.0M (BBPC) (Bundle)", "Stx StaffPack 1.0M (BBPC) (Bundle)", "Stx StaffPack 2.0M (BBPC) (Bundle)",
                                        "Stx StaffPack 4.0M (BBPC) (Bundle)", "Stx StaffPack (home prepaid) 1.0M (BBPC) (Bundle)", "Stx StaffPack (home prepaid) 2.0M (BBPC) (Bundle)",
                                        "Stx StaffPack (home prepaid) 4.0M (BBPC) (Bundle)", "UniFi Lite 10Mbps HE Lite TM Staff + STD20 (Bundle)",
                                        "UnF Lite 10Mbps HE Lite TM Staff +  STD20 (Bundle)", "UnF Lite 10Mbps STB TM Staff +  STD20 (Bundle)",
                                        "VIP 5 (TM Staff)", "VIP 10 (TM Staff)", "VIP 20 (TM Staff)", "VIP 5 (TM New Staff Package)", "VIP 10 (TM New Staff Package)",
                                        "VIP 20 (TM New Staff Package)", "VIP 5 (TM Existing Staff Package)", "VIP 10 (TM Existing Staff Package)", "VIP 20 (TM Existing Staff Package)",
                                        "UniFi Pro 100Mbps TM Staff + STD20", "UniFi Advance Plus 50Mbps TM Staff + STD20", "UniFi Advance 30Mbps TM Staff + STD20",
                                        "UniFi Lite 10Mbps HE Lite TM Staff + STD20", "UniFi Lite 10Mbps STB TM Staff +  STD20"
                                    StaffFlag = 1
                                Case Else
                                    StaffFlag = 0
                            End Select

                            If ValidateUserResp.MobileNumber IsNot Nothing Then
                                MobileNumber = ValidateUserResp.MobileNumber
                            Else
                                MobileNumber = ""
                            End If

                            If ValidateUserResp.CustomerEmail IsNot Nothing Then
                                Email = ValidateUserResp.CustomerEmail
                            Else
                                Email = ""
                            End If

                            FileInput = MyAccountIDs(x).TM_AccountID & "|" & FullName & "|" & Id.Replace("-", "") & "|" & FullAddress & "|" & StaffFlag & "|" & IdType & "|" & MobileNumber & "|" & Email
                        Else
                            FileInput = MyAccountIDs(x).TM_AccountID & "|||||||"
                        End If
                    Else

                        If ValidateUserResp.ResponseCode = "0" And UCase(ValidateUserResp.Status) = "ACTIVE" Then

                            FileInput = MyAccountIDs(x).TM_AccountID & "," & ValidateUserResp.AccountName & "," & ValidateUserResp.HouseNo & "," & ValidateUserResp.BuildingName & "," & ValidateUserResp.StreetType
                            FileInput += "," & ValidateUserResp.StreetName & "," & ValidateUserResp.Section & "," & ValidateUserResp.PostalCode & "," & ValidateUserResp.City & "," & ValidateUserResp.StateName
                            CountTotal += 1
                        Else
                            FileInput = MyAccountIDs(x).TM_AccountID & ",,,,,,,,,"
                            If Trim(AccountWithError <> "") Then
                                AccountWithError = AccountWithError & "," & MyAccountIDs(x).TM_AccountID
                            Else
                                AccountWithError = MyAccountIDs(x).TM_AccountID
                            End If
                        End If
                    End If

                Else
                    If ShowID <> 0 Then
                        FileInput = MyAccountIDs(x).TM_AccountID & "|||||||"
                    Else
                        FileInput = MyAccountIDs(x).TM_AccountID & ",,,,,,,,,"
                        If Trim(AccountWithError <> "") Then
                            AccountWithError = AccountWithError & "," & MyAccountIDs(x).TM_AccountID
                        Else
                            AccountWithError = MyAccountIDs(x).TM_AccountID
                        End If
                    End If
                End If
                LogFile(FilePath, FileInput)
            Next

            FileExists = File.Exists(FilePath)
            If FileExists Then

                Dim LogPath As String = String.Format(LocalLogPath & "log_{0}.txt", FileDate)
                LogFile(LogPath, DateTime.Now.ToString() & " - " & Filename)

                If ShowID <> 0 Then

                    Dim FtpStatus As String = ""
                    Dim ApiStatus As String = "Failed"

                    'FTP the file
                    Dim RemoteIp As String = "ftp://10.45.96.44/"
                    Dim RemoteTargetPath As String = "/GameShowArchiving/Production/OMG/new/"
                    Dim FtpUser As String = "ftpuser44"
                    Dim FtpPwd As String = "typ3ftp%44"

                    Dim request As FtpWebRequest = CType(WebRequest.Create(RemoteIp & RemoteTargetPath & Filename), FtpWebRequest)
                    request.Method = WebRequestMethods.Ftp.UploadFile
                    request.Credentials = New NetworkCredential(FtpUser, FtpPwd)
                    Dim sourceStream As StreamReader = New StreamReader(LocalSourcePath & Filename)

                    Dim fileContents As Byte() = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd())
                    sourceStream.Close()
                    request.ContentLength = fileContents.Length
                    Dim requestStream As Stream = request.GetRequestStream()
                    requestStream.Write(fileContents, 0, fileContents.Length)
                    requestStream.Close()
                    Dim response As FtpWebResponse = CType(request.GetResponse(), FtpWebResponse)

                    If response.StatusCode = "226" Then

                        'Move file
                        Dim FileMovePath As String = String.Format(LocalMovePath & "{0}", Filename)
                        System.IO.File.Move(FilePath, FileMovePath)

                        FtpStatus = "FTP Status= " & response.StatusCode & "-" & response.StatusDescription

                        'Trigger API to inform file is ready
                        Dim MerchantId As String = "VasGame"
                        Dim MerchantPwd As String = "%Fuj1568T"
                        Dim RandomId As Integer = MyOMGFunction.GenerateRandomInteger()

                        Dim b4hashstring As String = "##" & MerchantId & "##" & MerchantPwd & "##" & RandomId & "##"
                        Dim MySignature As String = MyOMGFunction.GetEncryption(b4hashstring)

                        Dim WS_GS As New GS.GameWS
                        Dim MyUpdateWinnerReq As New GS.UpdateWinnerRequest
                        Dim MyUpdateWinnerResp As New GS.UpdateWinnerResponse

                        MyUpdateWinnerReq.Signature = MySignature
                        MyUpdateWinnerReq.Merchant = MerchantId
                        MyUpdateWinnerReq.RequestID = RandomId.ToString()
                        MyUpdateWinnerReq.filename = Filename
                        MyUpdateWinnerReq.SubmitDatetime = DateTime.Now.ToString("G")

                        MyUpdateWinnerResp = WS_GS.UpdateWinner(MyUpdateWinnerReq)

                        ApiStatus = "API Status= " & MyUpdateWinnerResp.Status & "-" & MyUpdateWinnerResp.Description
                    Else
                        FtpStatus = "FTP Status Failed= " & response.StatusCode & "-" & response.StatusDescription
                    End If

                    LogFile(LogPath, ApiStatus)
                    LogFile(LogPath, FtpStatus, True)
                Else
                    Dim BulkStatus As String = ""

                    If CountTotal <> MyAccountIDs.Length Then
                        BulkStatus = "Bulk Status= Total: " & MyAccountIDs.Length & ", Total Insert: " & CountTotal & ", With error: " & AccountWithError
                    Else
                        BulkStatus = "Bulk Status= " & CountTotal
                    End If
                    LogFile(LogPath, BulkStatus)
                End If
            Else
                'Error file tak exist
            End If
        Else
            'Do nothing
        End If

        ' ================= EMERGENCY CASE ==================================================================
        ''Trigger API to inform file is ready
        'Dim FileDate As String = DateTime.Now.ToString("yyyyMMdd")
        'Dim Filename As String = ""
        'Filename = "OMG_" & ShowID & "_" & FileDate & ".txt"

        ''Move file
        'Dim LocalSourcePath As String = "E:\www\OMG_prod\GameShowArchiving\new\"
        'Dim LocalMovePath As String = "E:\www\OMG_prod\GameShowArchiving\sent\"
        'Dim FileMovePath As String = String.Format(LocalMovePath & "{0}", Filename)
        'Dim FilePath As String = String.Format(LocalSourcePath & "{0}", Filename)
        'System.IO.File.Move(FilePath, FileMovePath)

        'Dim MerchantId As String = "VasGame"
        'Dim MerchantPwd As String = "%Fuj1568T"
        'Dim RandomId As Integer = MyOMGFunction.GenerateRandomInteger()

        'Dim b4hashstring As String = "##" & MerchantId & "##" & MerchantPwd & "##" & RandomId & "##"
        'Dim MySignature As String = MyOMGFunction.GetEncryption(b4hashstring)

        'Dim WS_GS As New GS.GameWS
        'Dim MyUpdateWinnerReq As New GS.UpdateWinnerRequest
        'Dim MyUpdateWinnerResp As New GS.UpdateWinnerResponse

        'MyUpdateWinnerReq.Signature = MySignature
        'MyUpdateWinnerReq.Merchant = MerchantId
        'MyUpdateWinnerReq.RequestID = RandomId.ToString()
        'MyUpdateWinnerReq.filename = Filename
        'MyUpdateWinnerReq.SubmitDatetime = DateTime.Now.ToString("G")

        'MyUpdateWinnerResp = WS_GS.UpdateWinner(MyUpdateWinnerReq)

        'Dim ApiStatus As String = "Failed"
        'ApiStatus = "API Status= " & MyUpdateWinnerResp.Status & "-" & MyUpdateWinnerResp.Description

        'Dim LocalLogPath As String = "E:\www\OMG_prod\GameShowArchiving\log\"
        'Dim LogPath As String = String.Format(LocalLogPath & "log_{0}.txt", FileDate)
        'LogFile(LogPath, ApiStatus)
    End Sub

    Function LogFile(ByVal FilePath As String, ByVal input As String, Optional ByVal nextline As Boolean = False) As String

        Dim FileExists As Boolean
        FileExists = File.Exists(FilePath)

        If Not FileExists Then
            Using writer As New StreamWriter(FilePath)
                Try
                    writer.WriteLine(input)
                    If nextline = True Then
                        writer.WriteLine("")
                    End If
                    writer.Close()
                    Return "OK"
                Catch exFile As Exception
                    Return "Fail"
                End Try
            End Using
        Else
            Using writer As New StreamWriter(FilePath, True)
                Try
                    writer.WriteLine(input)
                    If nextline = True Then
                        writer.WriteLine("")
                    End If
                    writer.Close()
                    Return "OK"
                Catch ex As Exception
                    Return "Fail"
                End Try
            End Using
        End If

    End Function



End Class
