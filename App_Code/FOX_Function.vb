﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Runtime.Serialization.Json


Public Class FOX_Function

    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)

    '***FOX Cancel Subscription***
    '***Created By : Husna 23 Dec 2019 ***
    Function FOXCancelSubscription(ByVal TM_AccountId As String, productName As String, notificationType As String, expiryDate As String) As FOXCancelResponse

        'JSON Seriallizer/Deseriallizer
        Dim js As New JavaScriptSerializer()

        Dim MyRequest As New FOXCancelRequest
        Dim MyRequestElement As New FOXCancelRequestElement
        Dim MyResponse As New FOXCancelResponse
        Dim JSONstrResponse As String = ""


        'Convert Date to Milisecond
        Dim DateValue As Date = Date.Parse(expiryDate)
        Dim AsDate As DateTime = Convert.ToDateTime(DateValue)
        'before .net 4.6
        Dim ExpiryDateLong As Long = (AsDate.ToUniversalTime() - New DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds
        'after .net 4.6
        'Dts.ToUnixTimeMilliseconds
        'Dim ExpiryDateLong As Long =Dim Dts As New DateTimeOffset(AsDate)

        'Response Element
        MyRequestElement.userID = TM_AccountId
        MyRequestElement.productName = productName
        MyRequestElement.notificationType = notificationType
        MyRequestElement.expiryDate = ExpiryDateLong
        MyRequest.SubscriptionNotificationRequestMessage = MyRequestElement


        'Sample JSON Request Format
        ' "{'SubscriptionNotificationRequestMessage' {'userID':'" foxtest_tm@unifi"','productName':'TM','notificationType':'CANCEL,'expiryDate':'1576780200000'}}"
        'Seriallizer JSON
        Dim JSONstrRequest As String = js.Serialize(MyRequest)

        'URL
        Dim FOXUrl As String = "https://rest-dev.evergent.com/foxasia/subscriptionNotification"
        'Create a request using a URL that can receive a post. 
        Dim MyWebRequest As HttpWebRequest = WebRequest.Create(FOXUrl)
        ' Set the Method property of the request to POST.
        MyWebRequest.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        MyWebRequest.ContentType = "application/json"

        ' Create POST data and convert it to a byte array.
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(JSONstrRequest)
        ' Set the ContentLength property of the WebRequest.
        MyWebRequest.ContentLength = byteArray.Length

        Try
            ' Get the request stream.
            Dim ReqDataStream As Stream = MyWebRequest.GetRequestStream()
            ' Write the data to the request stream.
            ReqDataStream.Write(byteArray, 0, byteArray.Length)
            ' Close the Stream object.
            ReqDataStream.Close()

            Dim MyWebResponse As HttpWebResponse = MyWebRequest.GetResponse()
            If MyWebResponse.StatusCode = HttpStatusCode.OK Then

                ' MyWebResponse.ContentType = "application/json"
                ' Get the stream containing content returned by the server.
                Dim ResDataStream As Stream = MyWebResponse.GetResponseStream()
                ' Open the stream using a StreamReader for easy access.
                Dim Reader As New StreamReader(ResDataStream)
                ' Read the content.
                JSONstrResponse = Reader.ReadToEnd()
                ' Display the content.
                'Dim StrResponseResult As String = MyWebResponse.GetResponseHeader(ResponseValue)

                'Sample JSON Response Format
                '{"SubscriptionNotificationResponseMessage":{"message":"Success","responseCode":"1"}}
                'JSON DeserializeObject
                Dim dict As Object = js.DeserializeObject(JSONstrResponse)

                For Each item As Object In dict
                    MyResponse.Message = dict("SubscriptionNotificationResponseMessage")("message").ToString
                    MyResponse.ResponseCode = dict("SubscriptionNotificationResponseMessage")("responseCode").ToString
                Next

                ' Cleanup the streams and the response.
                Reader.Close()
                ResDataStream.Close()
                MyWebResponse.Close()

            Else
                MyResponse.ResponseCode = "-1"
                MyResponse.Message = "target URL is not reached"
            End If
        Catch ex As Exception
            MyResponse.ResponseCode = "-2"
            MyResponse.Message = ex.Message

        End Try

        'log the request and response
        Dim myLogObject As New APILog
        myLogObject.TM_AccountId = TM_AccountId
        myLogObject.EventName = "FOXCancelSubscription"
        myLogObject.Format = "JSON"
        myLogObject.Request = JSONstrRequest
        myLogObject.Response = JSONstrResponse
        myLogObject.Message = MyResponse.Message

        'call the function
        CancelSubscriptionAPILog(myLogObject)

        Return MyResponse

    End Function

    Function CancelSubscriptionAPILog(ByVal MyRequest As APILog) As Integer

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            'call stored procedure
            Dim cmd As New SqlCommand("CancelSubscriptionAPILog", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountId
            cmd.Parameters.Add("@EventName", SqlDbType.VarChar, 30).Value = MyRequest.EventName
            cmd.Parameters.Add("@Format", SqlDbType.VarChar, 10).Value = MyRequest.Format

            If Trim(MyRequest.Request) <> "" Then
                cmd.Parameters.Add("@DataReq", SqlDbType.VarChar, -1).Value = MyRequest.Request
            End If
            If Trim(MyRequest.Response) <> "" Then
                cmd.Parameters.Add("@DataResp", SqlDbType.VarChar, -1).Value = MyRequest.Response
            End If

            If Trim(MyRequest.Message) <> "" Then
                cmd.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = MyRequest.Message
            End If

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            'Do nothing
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try
    End Function

End Class

