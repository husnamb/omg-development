﻿
Public Class FOX_Object

End Class

'***FOX Cancel Subscription***
'***Created By : Husna 23 Dec 2019 ***
Public Class FOXCancelRequest
    Public SubscriptionNotificationRequestMessage As FOXCancelRequestElement
End Class

Public Class FOXCancelRequestElement
    Public userID As String
    Public productName As String
    Public notificationType As String
    Public expiryDate As Long
End Class

Public Class FOXCancelResponse
    Public Message As String
    Public ResponseCode As String
End Class

Public Class APILog
    Public TM_AccountId As String
    Public EventName As String
    Public Format As String
    Public TransDate As String
    Public Request As String
    Public Response As String
    Public Message As String
End Class

